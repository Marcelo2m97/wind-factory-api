def convert_phases_elm_lod(fases):
    """
    Convierte las fases de los elementos lod, de formato
    Windmil a formato PowerFactory.
    :param fases: Numero de fases en formato Windmil.
    :return: Numero de fases en formato PowerFsactoru.
    """
    switcher = {
        1: '8',
        2: '8',
        3: '8',
        4: '5',
        5: '5',
        6: '5',
        7: '3'
    }
    return switcher.get(fases, 1)
