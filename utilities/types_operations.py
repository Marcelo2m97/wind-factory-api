import app_constants as constants


def is_of_type(types_table, value):
    return len(types_table.loc[types_table['loc_name(a:40)'] == value, 'ID(a:40)'].values) > 0


def get_type(types_table, value):
    try:
        return types_table.loc[types_table['loc_name(a:40)']
                               == value, 'ID(a:40)'].values[0]
    except IndexError:
        print(
            '\n====================== ERROR!: Error en obtener elemento por su nombre: no se encontro ningun registro con el nombre de:',
            value,
            ' Por favor verificar archivo STD y confirmar que el respectivbo tipo exista en el archivo SEQ para este elemento', flush=True)
        print('Tabla utilizada para la busqueda:\n', types_table, flush=True)
        print('\n=================================', flush=True)
        exit(32)


def get_typ_lod(types, fases):
    typ = types.loc[types['phtech(i)'] == fases]['ID(a:40)']
    return typ.values[0]    

def convert_on_off_coup_type_6(value):
    switcher = {
        'C': '1',
        'O': '0'
    }

    return switcher.get(str(value), '1')


def convert_on_off_coup_type_10(value):
    switcher = {
        0: '1',
        1: '0'
    }

    return switcher.get(int(value), '1')


def modify_typ_voltreg(voltage, types_table, value):
    types_table.loc[types_table['loc_name(a:40)'] == value, 'Un(r)'] = str(
        voltage)
    return


def modify_typ_lne(types_table, loc_name, uline, nneutral, nlnph, future_id):
    if types_table.loc[types_table['loc_name(a:40)'] == loc_name, 'uline(r)'].values[0] == uline and \
            types_table.loc[types_table['loc_name(a:40)'] == loc_name, 'nneutral(i)'].values[0] == nneutral and \
            types_table.loc[types_table['loc_name(a:40)'] == loc_name, 'nlnph(i)'].values[0] == nlnph:
        return
    else:
        if types_table.loc[types_table['loc_name(a:40)'] == loc_name, 'uline(r)'].values[0] == constants.DEFAULT_EMPTY:
            types_table.loc[types_table['loc_name(a:40)']
                            == loc_name, 'uline(r)'] = uline
            if types_table.loc[types_table['loc_name(a:40)'] == loc_name, 'nneutral(i)'].values[0] == constants.DEFAULT_EMPTY:
                types_table.loc[types_table['loc_name(a:40)']
                                == loc_name, 'nneutral(i)'] = nneutral
                if types_table.loc[types_table['loc_name(a:40)'] == loc_name, 'nlnph(i)'].values[0] == constants.DEFAULT_EMPTY:
                    types_table.loc[types_table['loc_name(a:40)']
                                    == loc_name, 'nlnph(i)'] = nlnph
                    return
        else:
            print(
                '================================= ERROR! en metodo modificar_tipo_lne: No se encontro ningun tipo linea para: ',
                loc_name, '=================================', flush=True)
            exit(22)
    return
