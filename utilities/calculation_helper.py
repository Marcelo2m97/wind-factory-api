import app_constants as constants
import math

from utilities import elements_translator

def ConvertOhmMileToOhmKm(ohm_mile):
    return ohm_mile * constants.OHM_MILE_2_OHM_KM;


def convert_mlei_value(tipo):
    switcher = {
        12: "Al",
        2: "As",
        3: "Al",
        5: "Cu"
    }
    return switcher.get(tipo, "Al")


def getRowType(typ):
    switcher = {
        constants.TYPLNE_SUB_INT: "TypLne",
        constants.TYPFUSE_INT: "TypFuse",
        constants.TYPLOD_INT: "TypLod",
        constants.TYPTR2_INT: "TypTr2",
        constants.TYPSWITCH_INT: "TypSwitch",
        constants.TYPVOLTREG_INT: "TypVoltreg",
    }
    return switcher.get(typ, "TypLne")


def get_reactive_power(i, j, k):
    return (float(i) + float(j) + float(k)) / 1000


def get_out_serv_value(valor_windmil):
    if valor_windmil == 1 or valor_windmil == '1':
        return 0
    if valor_windmil == 0 or valor_windmil == '0':
        return 1


def get_load_classif(valor_windmil):
    switcher = {
        0: '1',
        1: '2',
        2: '2',
        3: '3'
    }

    return switcher.get(valor_windmil, '0')


def get_comparator_value(options_list):
    """
    Se utiliza cuando se puede utilizar cualquier valor de un grupo de valores,
    es decir si se puede usar cualquier valor entre la columna O, P y Q.
    :param options_list: Lista de valores posibles,
    :return: El primer elemento diferente de nulo.
    """
    for option in options_list:
        if str(option) != '' and str(option) != 'nan':
            return str(option)
    return ''


def multiply_by_root3(value):
    return str(float(value) * math.sqrt(3))


def convert_hv_side_l(value):
    switcher = {
        1: 'YN',
        2: 'YN'
    }

    return switcher.get(int(value), 'YN')


def convert_hv_side_h(value):
    switcher = {
        1: 'Y',
        2: 'D'
    }

    return switcher.get(int(value), 'D')


def convert_tr2_phases(value):
    """
    Convierte el valor de las fases para los transformadores de su formato en Windmil a
    el formato esperado en PowerFactory.
    :param value: Representacion de las fases para los transformadoes en Windmil.
    :return: Representacion de fases para transformadores en PowerFactory.
    """
    switcher = {
        1: '2',
        2: '2',
        3: '2',
        7: '3',
    }

    return switcher.get(int(value), '2')

def convert_fases(value):
    """
    Convierte el valor de las fases para todos los elementos (menos para transformadores)
    de su formato en Windmil a el formato esperado en PowerFactory.
    :param value: Representacion de las fases para los transformadoes en Windmil.
    :return: Representacion de fases para transformadores en PowerFactory.
    """
    switcher = {
        1: '1',
        2: '1',
        3: '1',
        4: '2',
        5: '2',
        6: '2',
        7: '3',
    }

    return switcher.get(int(value), '1')

def get_in_air_value(value):
    """
    Convierte el valor del tipo de linea a su respectivo valor
    de in_air.
    :param value: Valor que representa el tipo de linea en Windmil.
    :return: 0 si e linea subterranea y uno si es linea aerea
    """
    switcher = {
        1: '1',
        3: '0'
    }

    return switcher.get(int(value), '0')


def convert_terminal_phase_tech(value):
    """
    Convierte de el valor de fases en windmil a su respectiva representacion
    en PowerFactory para los elementos term.
    :param value: Valor de fases en Windmil
    :return: Valor de fases para los terminales en PowerFactory.
    """
    switcher = {
        1: '1PH-N',
        2: '1PH-N',
        3: '1PH-N',
        4: '2PH-N',
        5: '2PH-N',
        6: '2PH-N',
        7: 'ABC-N',
    }
    return switcher.get(value, '')

def get_obj_bus(value, default, obj_id, is_root, elm_tr2_table):
    switcher = {
        constants.ELM_LOD: '0',
        constants.ELM_SHNT: '0',
        constants.ELM_XNET: '0'
    }
    result = switcher.get(value, default)

    if result == default:
        if value == constants.TYPTR2:
            is_reduct = elements_translator.get_value_from_table(value=int(obj_id), table=elm_tr2_table,
                                                                 comparator_column_name='ID(a:40)',
                                                                 result_column_name='IsReductor',
                                                                 stop_on_error=True)
            if not is_root:
                if is_reduct:
                    return '0'
                else:
                    return '1'
            else:
                if is_reduct:
                    return '1'
                else:
                    return '0'
        else:
            if is_root:
                return '1'
            else:
                return '0'

    return result

def get_lowest_coord(std_file):
    lowest_values = std_file.min()
    lowest_coord_x = lowest_values['F']
    lowest_coord_y = lowest_values['G']
    return [lowest_coord_x, lowest_coord_y]

def get_sys_name(tipo, elmcoup_data, p_data_obj):
    """
    A partir del numero que representa el tipo de elemento en Windmil se convierte a
    su respectivo nombre de sistema.
    :param tipo: Numero que representa el tipo en windmil.
    :return: Nombre de sistema en tabla de equivalencias de PowerFactory.
    """
    switcher = {
        constants.ELM_LNE_SUB: 'd_lin',
        constants.ELM_LNE_AIR: 'd_lin',
        constants.ELM_VOLTREG: 'd_voltreg',
        constants.TYPFUSE: 'd_fuse',
        constants.TYPTR2: 'd_tr2',
        constants.ELM_LOD: 'd_load',
        constants.ELM_SHNT: 'd_shunt',
        constants.ELM_XNET: 'd_net'
    }

    sys_name = switcher.get(str(tipo), 'null')
    if sys_name == 'null' and tipo in constants.ELM_COUP_GROUP:
        sys_name = 'd_couple'
    if sys_name == 'd_fuse':
        if elmcoup_data.loc[elmcoup_data['ID(a:40)']
                            == p_data_obj, 'ID(a:40)'].values == p_data_obj:
            sys_name = 'd_couple'
    return sys_name


def get_type_from_sys_name(sys_name):
    """
    Convierte el nombre de sistema del elemento a su respectiva representacion de
    tipo dentro de Windmil.
    :param sys_name: Nombre de sistema para el elemento segun tabla de representacion de PowerFactory
    :return: Numero que representa el tipo de elemnto segun Windmil.
    """
    switcher = {
        'd_lin': constants.ELM_LNE_AIR,
        'd_voltreg': constants.ELM_VOLTREG,
        'd_fuse': constants.TYPFUSE,
        'd_tr2': constants.TYPTR2,
        'd_load': constants.ELM_LOD,
        'd_shunt': constants.ELM_SHNT,
        'd_net': constants.ELM_XNET
    }

    typ = switcher.get(sys_name, 'null')
    if typ == 'null' and sys_name == 'd_couple':
        typ = constants.ELM_COUP_GROUP[0]
    return typ


def get_root(table, root_name):
    """
    Retorna la fila del padre (Segun aparece en el archivo STD) del elemento ingresado.
    :param table: tabla del archivo STD
    :param root_name: Nombre del padre del elemento ingresado.
    :return: Los valores de la fila (fila del archivo STD) del elemento padre.
    """
    return table.loc[table['A'] == root_name].values


def get_valid_value(options_list):
    """
    Se utiliza para determinar el valor del voltaje linea linea para el elemento transformador.
    Dependiendo de las versiones este valor puede estar en la columna M o N. (K o L si no se han agregado
    las columnas correspondientes a la lon y lat).
    :param options_list: Lista de valores posibles,
    :return: El primer elemento diferente de 0.
    """
    for option in options_list:
        if float(option) != 0:
            return str(option)
    print(
        '================================= \nERROR! en funcion get_valid_value: No se encontro ningun valor de voltaje valido para el elemento,',
        'revisar columnas M y N del archivo STD para el registro correspondiente a este elemento.',
        '\n=================================', flush=True)
    print('ERROR:No se encontro ningun valor de voltaje valido para el elemento, revisar columnas M y N del archivo STD para el registro correspondiente a este elemento.', flush=True)
    exit(30)
