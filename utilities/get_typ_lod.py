def get_typ_lod(types, fases):
    """
    Obtiene el typlod correspondiente al elemento.
    :param types: Tabla de tipos.
    :param fases: Numero de fases
    :return: ID del registro del tipo correspondiente.
    """

    # TODO: Check with Lito that returns empty, but types has a lod with phtech = 3
    typ = types.loc[types['phtech(i)'] == float(fases)]['ID(a:40)']
    return typ.values[0]
