def get_value_from_table(
        value,
        table,
        comparator_column_name,
        result_column_name,
        stop_on_error):
    """
    Se utiliza para obtener un valor deseado de una tabla deseada
    :param stop_on_error: Si el sistema debe de parar o no en caso de no encontrar un registro.
    :param value: Valor que se desea encontrar.
    :param table: Tabla en la que se realizara la busqueda.
    :param comparator_column_name: Nombre de la columna en la que se encuentra el valor que fue recibido.
    :param result_column_name: Nombre de la columna donde se encuentra el valor deseado.
    :return: El valor correspondiente a la columna deseada del registro encontrado.
    """
    try:
        return table.loc[table[comparator_column_name]
                        == value, result_column_name].values[0]
    except KeyError and IndexError:
        if stop_on_error:
            print(
                'ERROR en funcion obtener_valor_de_tabla: No se encontro ningun registro que cumpliera'
                'con las condiciones necesarias', flush=True)
            print(
                'Parametros obtenidos:\nNombre del elemento:', value,
                "\nColumna de comparacion en archivo SEQ:",
                comparator_column_name,
                "\nColumna con resultado en archivo SEQ:",
                result_column_name, flush=True)
            exit(25)
