from utilities import ids


def reset_ids():
    ids.global_id = 1
    ids.typ_fuse_id = 0
    ids.typ_lne_id = 0
    ids.typ_lod_id = 0
    ids.typ_switch_id = 0
    ids.typ_tr2_id = 0
    ids.typ_voltreg_id = 0
    ids.elm_shnt_id = 0
    ids.elm_net_id = 0
    ids.int_grf_net_id = 0
    ids.elm_xnet_id = 0
    ids.elm_lod_id = 0
    ids.elm_tr2_id = 0
    ids.rel_fuse_id = 0
    ids.elm_coup_id = 0
    ids.elm_voltreg_id = 0
    ids.elm_lne_id = 0
    ids.elm_term_id = 0
    ids.sta_cubic_id = 0
    ids.int_grf_id = 0
    ids.int_grfcon_id = 0
