import app_constants as constants

from utilities import calculation_helper as calc

from utilities.get_value_from_table import get_value_from_table
from utilities.int_grf_operations import create_int_grf_4_terminal
from utilities.int_grf_con_operations import get_coord_r0p1_elm_net, get_coord_next_terminal, \
    get_coord_root_terminal, get_coord_r1p2_rel_fuse, get_coord_r2p3_rel_fuse, get_coord_r1p2_elm_coup,\
    get_coord_r2p3_elm_coup, get_coord_r1p2_elm_lod_shnt_tr2_voltreg, get_coord_r2p3_elm_tr2_voltreg


def create_int_grf(element_id, loc_name, id_int_grf_net, coord_x, coord_y, tipo, elements_table, comparator_value,
                   rot, table, index, term_table, elmcoup_data, lowest_center):
    """
    Crea el registro para cada elemento en la tabla de IntGrf.
    :param lowest_center: Coordenadas mas bajas en X y en Y.
    :param elmcoup_data: Tabla de elementos de proteccion.
    :param element_id: ID del elemento.
    :param loc_name: Nombre del elemento.
    :param id_int_grf_net: ID del registro de la tabla IntGrfNet.
    :param coord_x: Coordenada en X del centro del elemento WGS.
    :param coord_y: Coordenada en Y del centro del elemento WGS.
    :param tipo: Tipo de elemento.
    :param elements_table: Tabla correspondiente a todosl los elementos del mismo a tipo.
    :param comparator_value: Valor que se buscara en la tabla.
    :param rot: rotacion del elemento.
    :param table: Tabla en la que se creara el registro.
    :param index: Posicion en la que se va a crear el registro dentro de su respectiva tabla.
    :param term_table: Tabla de todos los terminales (ElmTerm).
    :return: Numero de registros creados.
    """
    root_id = get_value_from_table(comparator_value, elements_table, 'loc_name(a:40)', 'ID(a:40)', stop_on_error=True)
    sys_name = calc.get_sys_name(tipo=tipo, elmcoup_data=elmcoup_data, p_data_obj=root_id)
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': 'GraphNetObj_' + loc_name,
        'fold_id(p)': id_int_grf_net,
        'rCenterX(r)': coord_x - lowest_center[0],
        'rCenterY(r)': coord_y - lowest_center[1],
        'sSymNam(a:40)': sys_name,
        'pDataObj(p)': root_id,
        'iRot(i)': rot,
        'rSizeX(r)': 1,
        'rSizeY(r)': 1
    }

    table.loc[index] = new_entry

    if create_int_grf_4_terminal(element_id=element_id + 1, root_id=root_id, center_x=coord_x - lowest_center[0],
                                 center_y=coord_y - lowest_center[1], rot=rot, root_type=tipo,
                                 term_table=term_table, id_int_grf_net=id_int_grf_net, index=index + 1,
                                 table=table):
        return 2
    else:
        return 1


def create_int_grf_con(element_id, id_en_int_grf, rot, center, int_grf_table, table, index, term_table, p_data_obj,
                      tipo):
    if tipo == constants.ELM_XNET:
        print('Obteniendo coordenadas.', flush=True)
        r1p1 = get_coord_r0p1_elm_net(rot=rot, centro=center)
        print('Coordenada registro 1 punto 1:', r1p1, flush=True)
        r1p2 = get_coord_next_terminal(term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table,
                                           center=center, tipo=tipo, rot=rot)
        print('Coordenada registro 1 punto 2:', r1p2, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r1p1, r1=r1p2,
                                   table=table, index=index, num='1')
        print('----------------- Registro en IntGrfcon para ELM_XNET creado con exito.', flush=True)
        return 1
    if tipo == constants.TYPFUSE:
        # Registro 1
        print('Obteniendo coordenadas.', flush=True)
        r1p1 = get_coord_root_terminal(
            term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table, center=center, rot=rot, tipo=tipo)
        print('Coordenada registro 1 punto 1:', r1p1, flush=True)
        r1p2 = get_coord_r1p2_rel_fuse(rot=rot, center=center)
        print('Coordenada registro 1 punto 2:', r1p2, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r1p2, r1=r1p1,
                                   table=table, index=index, num='1')
        print('----------------- Registro 1 en IntGrfcon para elemento TYPFUSE creado con exito.', flush=True)
        element_id += 1
        index += 1
        # Registro 2
        r2p3 = get_coord_r2p3_rel_fuse(rot=rot, center=center)
        print('Coordenada registro 2 punto 3:', r2p3, flush=True)
        r2p4 = get_coord_next_terminal(term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table,
                                           center=center, tipo=tipo, rot=rot)
        print('Coordenada registro 2 punto 4:', r2p4, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r2p3, r1=r2p4,
                                   table=table, index=index, num='2')
        print(
            '----------------- Registro 2 en IntGrfcon para elemento TYPFUSE creado con exito.', flush=True)
        return 2
    if tipo in constants.ELM_COUP_GROUP:
        # Registro 1
        print('Obteniendo coordenadas.', flush=True)
        r1p1 = get_coord_root_terminal(
            term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table, center=center, rot=rot, tipo=tipo)
        print('Coordenada registro 1 punto 1:', r1p1, flush=True)
        r1p2 = get_coord_r1p2_elm_coup(rot=rot, center=center)
        print('Coordenada registro 1 punto 2:', r1p2, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r1p2, r1=r1p1,
                                   table=table, index=index, num='1')
        print('----------------- Registro 1 en IntGrfcon para elemento ELM_COUP_GROUP creado con exito.', flush=True)
        element_id += 1
        index += 1
        # Registro 2
        r2p3 = get_coord_r2p3_elm_coup(rot=rot, center=center)
        print('Coordenada registro 2 punto 3:', r2p3, flush=True)
        r2p4 = get_coord_next_terminal(term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table,
                                           center=center, tipo=tipo, rot=rot)
        print('Coordenada registro 2 punto 4:', r2p4, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r2p3, r1=r2p4,
                                   table=table, index=index, num='2')
        print(
            '----------------- Registro 2 en IntGrfcon para elemento ELM_COUP_GROUP creado con exito.', flush=True)
        return 2
    if tipo == constants.ELM_LNE_AIR or tipo == constants.ELM_LNE_SUB:
        # Registro 1
        print('Obteniendo coordenadas.', flush=True)
        r1p1 = get_coord_root_terminal(
            term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table, center=center, rot=rot, tipo=tipo)
        print('Coordenada registro 1 punto 1:', r1p1, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=center, r1=r1p1,
                                   table=table, index=index, num='1')
        print(
            '----------------- Registro 1 en IntGrfcon para elemento ELM_LNE_AIR o ELM_LNE_SUB creado con exito.', flush=True)
        element_id += 1
        index += 1
        # Registro 2
        r2p3 = get_coord_next_terminal(term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table,
                                           center=center, tipo=tipo, rot=rot)
        print('Coordenada registro 2 punto 3:', r2p3, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=center, r1=r2p3,
                                   table=table, index=index, num='2')
        print(
            '----------------- Registro 2 en IntGrfcon para elemento ELM_LNE_AIR o ELM_LNE_SUB creado con exito.', flush=True)
        return 2
    if tipo == constants.ELM_LOD or tipo == constants.ELM_SHNT:
        # Registro 1
        print('Obteniendo coordenadas.', flush=True)
        r1p1 = get_coord_root_terminal(
            term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table, center=center, rot=rot, tipo=tipo)
        print('Coordenada registro 1 punto 1:', r1p1, flush=True)
        r1p2 = get_coord_r1p2_elm_lod_shnt_tr2_voltreg(
            rot=rot, center=center)
        print('Coordenada registro 1 punto 2:', r1p2, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r1p2, r1=r1p1,
                                   table=table, index=index, num='1')
        print(
            '----------------- Registro 1 en IntGrfcon para elemento ELM_LOD o ELM_SHNT creado con exito.', flush=True)
        return 1
    if tipo == constants.TYPTR2:
        # Registro 1
        print('Obteniendo coordenadas.', flush=True)
        r1p1 = get_coord_root_terminal(
            term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table, center=center, rot=rot, tipo=tipo)
        print('Coordenada registro 1 punto 1:', r1p1, flush=True)
        r1p2 = get_coord_r1p2_elm_lod_shnt_tr2_voltreg(
            rot=rot, center=center)
        print('Coordenada registro 1 punto 2:', r1p2, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r1p2, r1=r1p1,
                                   table=table, index=index, num='1')
        print('----------------- Registro 1 en IntGrfcon para elemento TYPTR2 creado con exito.', flush=True)
        element_id += 1
        index += 1
        # Registro 2
        r2p3 = get_coord_r2p3_elm_tr2_voltreg(rot=rot, center=center)
        print('Coordenada registro 2 punto 3:', r2p3, flush=True)
        r2p4 = get_coord_next_terminal(term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table,
                                           center=center, tipo=tipo, rot=rot)
        print('Coordenada registro 2 punto 4:', r2p4, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r2p3, r1=r2p4,
                                   table=table, index=index, num='2')
        print(
            '----------------- Registro 2 en IntGrfcon para elemento TYPTR2 creado con exito.', flush=True)
        return 2
    if tipo == constants.ELM_VOLTREG:
        # Registro 1
        print('Obteniendo coordenadas.', flush=True)
        r1p1 = get_coord_root_terminal(
            term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table, center=center, rot=rot, tipo=tipo)
        print('Coordenada registro 1 punto 1:', r1p1, flush=True)
        r1p2 = get_coord_r1p2_elm_lod_shnt_tr2_voltreg(
            rot=rot, center=center)
        print('Coordenada registro 1 punto 2:', r1p2, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r1p2, r1=r1p1,
                                   table=table, index=index, num='1')
        print('----------------- Registro 1 en IntGrfcon para elemento ELM_VOLTREG creado con exito.', flush=True)
        element_id += 1
        index += 1
        # Registro 2
        r2p3 = get_coord_r2p3_elm_tr2_voltreg(rot=rot, center=center)
        print('Coordenada registro 2 punto 3:', r2p3, flush=True)
        r2p4 = get_coord_next_terminal(term_table=term_table, p_data_obj=p_data_obj, int_grf_table=int_grf_table,
                                           center=center, tipo=tipo, rot=rot)
        print('Coordenada registro 2 punto 4:', r2p4, flush=True)
        create_int_grf_con_record(element_id=element_id, id_en_int_grf=id_en_int_grf, r0=r2p3, r1=r2p4,
                                   table=table, index=index, num='2')
        print(
            '----------------- Registro 2 en IntGrfcon para elemento ELM_VOLTREG creado con exito.', flush=True)
        return 2
    return 0

def create_int_grf_con_record(element_id, id_en_int_grf, r0, r1, table, index, num):
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': 'GC0_' + num,
        'fold_id(p)': id_en_int_grf,
        'rX:SIZEROW(i)': 2,
        'rX:0(r)': r0[0],
        'rX:1(r)': r1[0],
        'rY:SIZEROW(i)': 2,
        'rY:0(r)': r0[1],
        'rY:1(r)': r1[1],
    }
    table.loc[index] = new_entry
    return
