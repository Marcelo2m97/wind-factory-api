import numpy
import pandas as pd
from pandas import Series

import app_constants as constants

from utilities.get_value_from_table import get_value_from_table

def get_coord_r2p3_elm_tr2_voltreg(rot, center):
    x0 = -1
    y0 = -1
    if int(rot) == 0:
        x0 = center[0]
        y0 = float(center[1]) - constants.UM_PF
    if int(rot) == 90:
        x0 = float(center[0]) + constants.UM_PF
        y0 = center[1]
    if int(rot) == 180:
        x0 = center[0]
        y0 = float(center[1]) + constants.UM_PF
    if int(rot) == 270:
        x0 = float(center[0]) - constants.UM_PF
        y0 = center[1]
    return [x0, y0]


def get_coord_r1p2_elm_lod_shnt_tr2_voltreg(rot, center):
    x0 = -1
    y0 = -1
    if int(rot) == 0:
        x0 = center[0]
        y0 = float(center[1]) + constants.UM_PF
    if int(rot) == 90:
        x0 = float(center[0]) - constants.UM_PF
        y0 = center[1]
    if int(rot) == 180:
        x0 = center[0]
        y0 = float(center[1]) - constants.UM_PF
    if int(rot) == 270:
        x0 = float(center[0]) + constants.UM_PF
        y0 = center[1]
    return [x0, y0]


def get_coord_r1p2_elm_coup(rot, center):
    x0 = -1
    y0 = -1
    if int(rot) == 0:
        x0 = center[0]
        y0 = float(center[1]) + (0.5 * constants.UM_PF)
    if int(rot) == 90:
        x0 = float(center[0]) - (0.5 * constants.UM_PF)
        y0 = center[1]
    if int(rot) == 180:
        x0 = center[0]
        y0 = float(center[1]) - (0.5 * constants.UM_PF)
    if int(rot) == 270:
        x0 = float(center[0]) + (0.5 * constants.UM_PF)
        y0 = center[1]
    return [x0, y0]


def get_coord_r2p3_elm_coup(rot, center):
    x0 = -1
    y0 = -1
    if int(rot) == 0:
        x0 = center[0]
        y0 = float(center[1]) - (0.5 * constants.UM_PF)
    if int(rot) == 90:
        x0 = float(center[0]) + (0.5 * constants.UM_PF)
        y0 = center[1]
    if int(rot) == 180:
        x0 = center[0]
        y0 = float(center[1]) + (0.5 * constants.UM_PF)
    if int(rot) == 270:
        x0 = float(center[0]) - (0.5 * constants.UM_PF)
        y0 = center[1]
    return [x0, y0]


def get_coord_r1p2_rel_fuse(rot, center):
    # Obtener valor del centro de la terminal padre
    x0 = -1
    y0 = -1
    if int(rot) == 0:
        x0 = center[0]
        y0 = float(center[1]) + (0.7 * constants.UM_PF)
    if int(rot) == 90:
        x0 = float(center[0]) - (0.7 * constants.UM_PF)
        y0 = center[1]
    if int(rot) == 180:
        x0 = center[0]
        y0 = float(center[1]) - (0.7 * constants.UM_PF)
    if int(rot) == 270:
        x0 = float(center[0]) + (0.7 * constants.UM_PF)
        y0 = center[1]
    return [x0, y0]


def get_coord_r2p3_rel_fuse(rot, center):
    # Obtener valor del centro de la terminal padre
    x0 = -1
    y0 = -1
    if int(rot) == 0:
        x0 = center[0]
        y0 = float(center[1]) - (0.7 * constants.UM_PF)
    if int(rot) == 90:
        x0 = float(center[0]) + (0.7 * constants.UM_PF)
        y0 = center[1]
    if int(rot) == 180:
        x0 = center[0]
        y0 = float(center[1]) + (0.7 * constants.UM_PF)
    if int(rot) == 270:
        x0 = float(center[0]) - (0.7 * constants.UM_PF)
        y0 = center[1]
    return [x0, y0]


def get_coord_r0p1_elm_net(rot, centro):
    x0 = -1
    y0 = -1
    if int(rot) == 0:
        x0 = centro[0]
        y0 = float(centro[1]) + constants.UM_PF
    if int(rot) == 90:
        x0 = float(centro[0]) - constants.UM_PF
        y0 = centro[1]
    if int(rot) == 180:
        x0 = centro[0]
        y0 = centro[1] - constants.UM_PF
    if int(rot) == 270:
        x0 = float(centro[0]) + constants.UM_PF
        y0 = centro[1]
    return [x0, y0]


def get_coord_next_terminal(term_table, p_data_obj, center, tipo, rot, int_grf_table):
    next_term_id = get_next_terminal_id(
        term_table=term_table, p_data_obj=p_data_obj)
    if isinstance(next_term_id, int):
        x1 = get_value_from_table(value=next_term_id, table=int_grf_table, comparator_column_name='pDataObj(p)',
                                    result_column_name='rCenterX(r)', stop_on_error=False)
        y1 = get_value_from_table(value=next_term_id, table=int_grf_table, comparator_column_name='pDataObj(p)',
                                    result_column_name='rCenterY(r)', stop_on_error=False)
        return [x1, y1]
    else:
        return empty_coord()


def get_next_terminal_id(term_table, p_data_obj):
    if term_table.loc[term_table['RootID']
                      == p_data_obj, 'ID(a:40)'].values > 0:
        return get_value_from_table(value=p_data_obj, table=term_table, comparator_column_name='RootID',
                                      result_column_name='ID(a:40)', stop_on_error=False)
    else:
        return constants.DEFAULT_EMPTY


def get_coord_root_terminal(term_table, p_data_obj, int_grf_table, center, rot, tipo):
    root_term_id = get_root_terminal_id(
        term_table=term_table, p_data_obj=p_data_obj)
    if isinstance(root_term_id, int):
        x1 = get_value_from_table(value=root_term_id, table=int_grf_table, comparator_column_name='pDataObj(p)',
                                    result_column_name='rCenterX(r)', stop_on_error=True)
        y1 = get_value_from_table(value=root_term_id, table=int_grf_table, comparator_column_name='pDataObj(p)',
                                    result_column_name='rCenterY(r)', stop_on_error=True)
        return [x1, y1]
    else:
        return empty_coord()


def empty_coord():
    return [constants.DEFAULT_EMPTY, constants.DEFAULT_EMPTY]


def get_root_terminal_id(term_table, p_data_obj):
    df = term_table.loc[term_table['NodesIDs'].str.contains(
        str(p_data_obj), case=False)]
    root_term_id = get_root_id(df=df, p_data_obj=str(p_data_obj))
    if root_term_id != constants.DEFAULT_EMPTY or len(df.index) < 2:
        try:
            return int(root_term_id)
        except IndexError and ValueError:
            return [constants.DEFAULT_EMPTY, constants.DEFAULT_EMPTY]
    else:
        print('Se encontro un elemento que tiene mas de un registro para su terminal padre (', len(df.index), ')\nID del elemento:',
              p_data_obj,
              '\nRegistros en los que aparece como nodo hijo:', flush=True)
        print(df[['ID(a:40)', 'loc_name(a:40)', 'NodesIDs']], flush=True)
        exit(27)


def get_root_id(df, p_data_obj):
    try:
        temp_separated = pd.concat([Series(row['ID(a:40)'], row['NodesIDs'].split(','))
                                    for _, row in df.iterrows()]).reset_index()
        temp_separated.columns = ['1', '2']
        return get_value_from_table(value=p_data_obj, table=temp_separated, comparator_column_name='1',
                                      result_column_name='2', stop_on_error=True)
    except ValueError:
        return constants.DEFAULT_EMPTY
