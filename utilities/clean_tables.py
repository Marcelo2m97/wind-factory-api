import app_constants as constants

def clean_tables(elmTermData, typLneData, typTr2Data, typVoltregData, elmTr2Data):
  """
  Elimina todas las filas que representan tipos que no fueron utilizados en las tablas
  TypLne, TypTr2, TypVoltreg.
  Tambien elimina las columnas extras agregadas en la tabla ElmTerm.
  :return:
  """
  print('************************************************************ LIMPIANDO TABLAS ************************************************************', flush=True)
  elmTermData = clean_table(labels=[
    'RootID', 'Nodes', 'NodesIDs', 'RootType', 'NodesTypes'], table=elmTermData, axis=1)
  print('----------------- Tabla ElmTerm modificada con exito. Columnas eliminadas: RootID,',
    'Nodes,', 'NodesIDs,', 'RootType,', 'NodesTypes', flush=True)
  typLneData = typLneData[typLneData['uline(r)']
                                    != constants.DEFAULT_EMPTY]
  print('----------------- Tabla TypLne modificada con exito. Columnas eliminadas: uline(r)', flush=True)
  typTr2Data = typTr2Data[typTr2Data['utrn_h(r)']
                                    != constants.DEFAULT_EMPTY]
  print('----------------- Tabla TypTr2 modificada con exito. Columnas eliminadas: utrn_h(r)', flush=True)
  typVoltregData = typVoltregData[typVoltregData['Un(r)']
                                            != constants.DEFAULT_EMPTY]
  print('----------------- Tabla TypVoltreg modificada con exito. Columnas eliminadas: Un(r)', flush=True)
  elmTr2Data = clean_table(
    labels=['IsReductor'], table=elmTr2Data, axis=1)
  print('----------------- Tabla ElmTr2 modificada con exito. Columnas eliminadas: IsReductor', flush=True)
  typVoltregData = clean_table(
    labels=['usetp(r)'], table=typVoltregData, axis=1)
  print('----------------- Tabla TypVoltreg modificada con exito. Columnas eliminadas: usetp(r)', flush=True)
  
  return [
    elmTermData,
    typLneData,
    typTr2Data,
    typVoltregData,
    elmTr2Data
  ]


def clean_table(labels, table, axis):
  result_table = table
  try:
    result_table = table.drop(labels=labels, axis=axis)
  except KeyError:
    print("No se encontro ninguna columna con alguno de estos nombres:", labels, flush=True)
    print("Columnas de la tabla: ", result_table.columns, flush=True)
    print('Tabla utilizada para la busqueda:', result_table, flush=True)
    exit(27)
  return result_table
