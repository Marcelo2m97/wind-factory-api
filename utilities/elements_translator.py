from utilities import ids
from utilities import calculation_helper as calc

from utilities.get_value_from_table import get_value_from_table
from utilities.get_typ_lod import get_typ_lod
from utilities.convert_phases_elm_lod import convert_phases_elm_lod
from utilities.typ_tr2_operations import typ_tr2_exists, get_typ_tr2_id, get_typ_tr2_row, modify_typ_tr2
from utilities.typ_voltreg_operations import get_usetp_regulator
from utilities.typ_lne_operations import get_typ_lne_id, get_typ_lne_row, typ_lne_exists
from utilities.types_operations import is_of_type, get_type, modify_typ_voltreg
from utilities.get_root_voltage import get_root_voltage
from utilities import types_translator

import app_constants as constants
import re


def create_gral_info(table, version):
    """
    Crea el unico registro necesario en la tabla GralInfo
    :param element_id: Id del elemento
    :param table: Tabla en la que se agrgara el registro
    :param version: version de PowerFactory
    :return:
    """
    new_entry = {
        'ID(a:40)': ids.global_id,
        'Descr(a:40)': 'Version',
        'Val(a:40)': version
    }
    table.loc[0] = new_entry
    ids.global_id += 1

    return


def create_elm_net(table_elm_net, table_int_grf_net):
    """
    Crea el registro de ElmNet y al finalizar crea el registro de
    la tabla GrafNet.
    :param element_id: Elemento global del elemento
    :param table_elm_net: Tabla que se utilizara para ElmNet
    :param table_grf_net: Tabla que se utilizara para GrfNet
    :return: N/A
    """
    p_diagram = create_int_grf_net(
        element_id=ids.global_id + 1,
        id_elm_net=ids.global_id,
        table_int_grf_net=table_int_grf_net)
    new_entry = {
        'ID(a:40)': ids.global_id,
        'loc_name(a:40)': 'Grid',
        'frnom(r)': '60',
        'pDiagram(p)': p_diagram
    }

    table_elm_net.loc[0] = new_entry
    ids.global_id += 2
    print('----------------- Registro en ElmNet creado con exito', flush=True)
    return p_diagram


def create_int_grf_net(element_id, id_elm_net, table_int_grf_net):
    """
    Crea el unico registro existente dentro de la tabla de GrfNet.

    :param element_id: Elemento global del ID
    :param id_elm_net: ID del registro de ElmNet
    :param table_grf_net: Tabla que se utilizara para GrfNet
    :return: ID del registro creado en GrfNet.
    """
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': 'Graphicg',
        'snap_on(i)': '1',
        'grid_on(i)': '1',
        'ortho_on(i)': '1',
        'pDataFolder(p)': id_elm_net
    }

    table_int_grf_net.loc[0] = new_entry
    print('----------------- Registro en IntGrfnet creado con exito', flush=True)
    return element_id


def create_elm_shnt(
        element_id,
        index,
        loc_name,
        id_elm_net,
        tech,
        rated_voltage,
        rr_power,
        gps_lat,
        gps_lon,
        table):
    """
    Crea el registro de ElmShnt dentro de su respectiva tabla.
    :param element_id: ID global del elemento.
    :param index: Posicino dentro de la tabla.
    :param loc_name: Nombre identificador del elemento.
    :param id_elm_net: ID del registro de ElmNet.
    :param tech: Tecnologia del elemento, proviene del STD.
    :param rated_voltage: Voltaje correspondiente del elemento, proviene del STD.
    :param rr_power: Valor del reactive power del elemento, proviene del STD.
    :param table: Tabla en la que se va a introducir el registro.
    :return: N/A
    """
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'fold_id(p)': id_elm_net,
        'systp(i)': '0',
        'ctech(i)': tech,
        'shtype(i)': '2',
        'ushnm(r)': rated_voltage,
        'mode_inp(a:3)': 'DEF',
        'qcapn(r)': rr_power,
        'ncapx(i)': '1',
        'ncapa(i)': '1',
        'outserv(i)': '0',
        'tandc(r)': '0',
        'iswitch(i)': '0',
        'iTaps(i)': '0',
        'GPSlat(r)': gps_lat,
        'GPSlon(r)': gps_lon
    }
    table.loc[index] = new_entry
    print('----------------- Registro en ElmShnt creado con exito para:', loc_name, flush=True)
    return


def create_elmXnet(
        element_id,
        index,
        loc_name,
        id_elm_net,
        gps_lat,
        gps_lon,
        table,
        seq_table,
        seq_comparator_value):
    """
    Crea un registro para un elemento tipo 9 en el STD, correspondiente a
    ElmXNet, comunmente este registro representa una fuente.
    :param element_id: ID global del elemento.
    :param index: Posicion en la tabla.
    :param loc_name: Nombre identificador del elemento.
    :param id_elm_net: ID del registro en la tabla ElmNet.
    :param table: Tabla en la que se va agregar el registro.
    :param seq_table: Tabla correspondiente al archivo SEQ, proveniente de Windmil.
    :param seq_comparator_value: Valor de comparacion que se va a utilizar al momento de buscar el registro en
                                el archivo SEQ.
    :return: N/A
    """
    re_r = get_value_from_table(
        value=seq_comparator_value,
        table=seq_table,
        comparator_column_name='A',
        result_column_name='H',
        stop_on_error=True)
    xe_r = get_value_from_table(
        value=seq_comparator_value,
        table=seq_table,
        comparator_column_name='A',
        result_column_name='I',
        stop_on_error=True)
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'fold_id(p)': id_elm_net,
        'bustp(a:2)': 'PV',
        'pgini(r)': '0',
        'qgini(r)': '0',
        'usetp(r)': '1',
        'outserv(i)': '0',
        'Re(r)': re_r,
        'Xe(r)': xe_r,
        'uset_mode(i)': '0',
        'mode_inp(a:3)': 'DEF',
        'iintgnd(i)': '0',
        'cgnd(i)': '0',
        'cpeter(i)': '0',
        'Kpf(r)': '0',
        'K(r)': '0',
        'GPSlat(r)': gps_lat,
        'GPSlon(r)': gps_lon}
    table.loc[index] = new_entry
    print('----------------- Registro en ElmXnet creado con exito para:', loc_name, flush=True)
    return


def create_elmn_lod(
        element_id,
        index,
        loc_name,
        fases,
        id_elm_net,
        plini,
        qlini,
        out_service,
        classif,
        gps_lat,
        gps_lon,
        type_table,
        table):
    """
    Crea un registro para un elemnto de tipo lod.
    :param element_id: ID global del elemento.
    :param index: Posicion dentro de la tabla.
    :param loc_name: Nombre identificador del elemento.
    :param fases: Fases del elemento, proviene del archivo STD.
    :param id_elm_net: ID del registro en la tabla ElmNet.
    :param plini: Valor de active power, proviene del archivo STD.
    :param qlini: Valor de reactive power, proviene del archivo STD.
    :param out_service: Si se encuentra en servicio, proviene del archivo STD.
    :param classif: clasificacion del elemento, archivo STD.
    :param type_table: Tabla de tipos correspondientes a este elemento (TypLod).
    :param table: Tabla en la que se va a crear el registro.
    :return: N/A.
    """
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'fold_id(p)': id_elm_net,
        'typ_id(p)': get_typ_lod(
            types=type_table,
            fases=convert_phases_elm_lod(fases)),
        'mode_inp(a:3)': 'DEF',
        'plini(r)': plini,
        'qlini(r)': qlini,
        'scale0(r)': '1',
        'i_scale(i)': '1',
        'outserv(i)': out_service,
        'classif(a:20)': classif,
        'i_sym(i)': '0',
        'u0(r)': '1',
        'iLoadTrf(i)': '0',
        'GPSlat(r)': gps_lat,
        'GPSlon(r)': gps_lon
    }
    table.loc[index] = new_entry
    print('----------------- Registro en ElmLod creado con exito para:', loc_name, flush=True)
    return


def create_elm_tr2(element_id, loc_name, typ_loc_name, id_elm_net, nt2ph,
                   utrn_h, utrn_l, tr2cn_h, tr2cn_l, index, gps_lat, gps_lon, type_table, table):
    # Verificar numero de fases
    if nt2ph == '2':
        tr2cn_l = 'YN'
        tr2cn_h = 'YN'
    # Asignar voltajes dependiendo su funcion.
    # Reductor o elevador.
    if float(utrn_h) > float(utrn_l):
        high_voltage = utrn_h
        low_voltage = utrn_l
        is_reduct = 1
    else:
        high_voltage = utrn_l
        low_voltage = utrn_h
        is_reduct = 0
    # Verificar si ya existe un registro para este elemento.

    rows_created = 0
    if typ_tr2_exists(types_table=type_table, loc_name=typ_loc_name, nt2ph=nt2ph,
                      utrn_h=utrn_h, utrn_l=utrn_l):
        typ_id = get_typ_tr2_id(types_table=type_table, loc_name=typ_loc_name, nt2ph=nt2ph,
                                utrn_h=utrn_h, utrn_l=utrn_l)
    else:
        typ_id = create_typ_tr2_for_element(types=type_table, type_name=typ_loc_name, nt2ph=nt2ph,
                                            utrn_h=high_voltage, utrn_l=low_voltage, tr2cn_h=tr2cn_h,
                                            tr2cn_l=tr2cn_l, element_id=element_id)
        rows_created += 1
        element_id += 1

    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'fold_id(p)': id_elm_net,
        'typ_id(p)': typ_id,
        'ntnum(i)': '1',
        'outserv(i)': '0',
        'nntap(i)': '0',
        'iTaps(i)': '0',
        'maxload(r)': '100',
        'lossAssign(i)': '0',
        'ratfac(r)': '1',
        'cneutcon(i)': '0',
        'cpeter_l(i)': '0',
        'cgnd_l(i)': '0',
        're0tr_h(r)': '0',
        'xe0tr_h(r)': '0',
        'GPSlat(r)': gps_lat,
        'GPSlon(r)': gps_lon,
        'IsReductor': is_reduct
    }

    table.loc[index] = new_entry
    rows_created += 1
    print('----------------- Registro en ElmTr2 creado con exito para:', loc_name, flush=True)
    return rows_created


def create_typ_tr2_for_element(types, type_name, nt2ph, utrn_h, utrn_l, tr2cn_h, tr2cn_l, element_id):
    typ_tr2_model = get_typ_tr2_row(types_table=types, loc_name=type_name)
    types_translator.create_typ_tr2(element_id=element_id, loc_name=type_name,
                                    strn=typ_tr2_model['strn(r)'].values[0],
                                    index=int(types['ID(a:40)']
                                              [types.index[-1]]) + 1,
                                    table=types)
    modify_typ_tr2(
        typ=types,
        typ_id=element_id,
        nt2ph=nt2ph,
        utrn_h=utrn_h,
        utrn_l=utrn_l,
        tr2cn_h=tr2cn_h,
        tr2cn_l=tr2cn_l)
    return get_typ_tr2_id(types_table=types, loc_name=type_name, nt2ph=nt2ph,
                          utrn_h=utrn_h, utrn_l=utrn_l)


def create_rel_fuse(
        element_id,
        loc_name,
        id_elm_net,
        comparator_value,
        gps_lat,
        gps_lon,
        types_table,
        phases,
        index,
        table):
    if is_of_type(types_table=types_table, value=comparator_value):
        new_entry = {
            'ID(a:40)': element_id,
            'loc_name(a:40)': loc_name,
            'fold_id(p)': id_elm_net,
            'typ_id(p)': str(
                get_type(
                    types_table=types_table,
                    value=comparator_value)),
            'on_off(i)': '1',
            'aUsage(a:4)': 'fus',
            'outserv(i)': '0',
            'nphase(i)': calc.convert_fases(value=phases),
            'nneutral(i)': '1',
            'iphauto(i)': '0',
            'calcuse(i)': '0',
            'dev_no(i)': '1',
            'GPSlon(r)': gps_lon,
            'GPSlat(r)': gps_lat
        }
        table.loc[index] = new_entry
        print('----------------- Registro en RelFuse creado con exito para:', loc_name, flush=True)
        return True
    return False


def create_elm_coup(
        element_id,
        loc_name,
        id_elm_net,
        nphase,
        comparator_value,
        tipo,
        index,
        gps_lat,
        gps_lon,
        types_table,
        table):
    if comparator_value != constants.DEFAULT_EMPTY and is_of_type(
            types_table=types_table, value=comparator_value):
        new_entry = {
            'ID(a:40)': element_id,
            'loc_name(a:40)': loc_name,
            'fold_id(p)': id_elm_net,
            'typ_id(p)': str(
                get_type(
                    types_table=types_table,
                    value=comparator_value)),
            'on_off(i)': '1',
            'aUsage(a:4)': 'cbk',
            'nphase(i)': nphase,
            'nneutral(i)': '0',
            'GPSlat(r)': gps_lat,
            'GPSlon(r)': gps_lon,
        }
        table.loc[index] = new_entry
        print('----------------- Registro en ElmCoup creado con exito para:', loc_name, flush=True)
        return True
    else:
        if tipo == 6:
            new_entry = {
                'ID(a:40)': element_id,
                'loc_name(a:40)': loc_name,
                'fold_id(p)': id_elm_net,
                'typ_id(p)': '',
                'on_off(i)': '1',
                'aUsage(a:4)': 'cbk',
                'nphase(i)': nphase,
                'nneutral(i)': '0',
                'GPSlat(r)': gps_lat,
                'GPSlon(r)': gps_lon
            }
            table.loc[index] = new_entry
            print(
                '----------------- Registro en ElmCoup creado con exito para:', loc_name, flush=True)
            return True
    return False


def create_elm_voltreg(
        element_id,
        loc_name,
        id_elm_net,
        comparator_value,
        rated_voltage,
        index,
        gps_lat,
        gps_lon,
        types_table,
        table,
        fila_padre):
    if fila_padre.size != 0 and is_of_type(
            types_table=types_table, value=comparator_value):
        tipo_fila_padre = str(fila_padre[0][1])
        if tipo_fila_padre == constants.TYPLNE_AEREA or \
                tipo_fila_padre == constants.TYPLNE_SUB_STD:
            volt_padre = fila_padre[0][15]
            voltaje_padre = re.search(r"\d+(\.\d+)?", volt_padre)
            voltaje_padre = voltaje_padre.group(0)
            modify_typ_voltreg(
                voltage=float(voltaje_padre) * float(rated_voltage),
                types_table=types_table,
                value=comparator_value)
            new_entry = {
                'ID(a:40)': element_id,
                'loc_name(a:40)': loc_name,
                'fold_id(p)': id_elm_net,
                'outserv(i)': '0',
                'typ_id(p)': get_type(
                    types_table=types_table,
                    value=comparator_value),
                'maxload(r)': '100',
                'nntap1(i)': '0',
                'lossAssign(i)': '0',
                'i_pcReglim(i)': '0',
                'ratfac(r)': '1',
                'cneutcon(i)': '0',
                'cgnd(i)': '0',
                're0(r)': '0',
                'xe0(r)': '0',
                'ntrcn(i)': '0',
                'GPSlat(r)': gps_lat,
                'GPSlon(r)': gps_lon,
                'i_cont(i)': '0',
                'ilcph(i)': '0',
                'usetp(r)': get_usetp_regulator(types_table=types_table, value=comparator_value),
                'usp_up(r)': '1.11',
                'usp_low(r)': '0.9',
                'Tctrl(r)': '0.5',
                'ildc(r)': '0'
            }
            table.loc[index] = new_entry
            print(
                '----------------- Registro en ElmVoltreg creado con exito para:', loc_name, flush=True)
            return True
        elif tipo_fila_padre == constants.ELM_SHNT:
            volt_padre = fila_padre[0][13]
            voltaje_padre = re.search(r"\d+(\.\d+)?", volt_padre)
            voltaje_padre = voltaje_padre.group(0)
            voltaje_padre = float(voltaje_padre) / 1000
            modify_typ_voltreg(
                voltage=float(voltaje_padre) * float(rated_voltage),
                types_table=types_table,
                value=comparator_value)
            new_entry = {
                'ID(a:40)': element_id,
                'loc_name(a:40)': loc_name,
                'fold_id(p)': id_elm_net,
                'outserv(i)': '0',
                'typ_id(p)': get_type(
                    types_table=types_table,
                    value=comparator_value),
                'maxload(r)': '100',
                'nntap1(i)': '0',
                'lossAssign(i)': '0',
                'i_pcReglim(i)': '0',
                'ratfac(r)': '1',
                'cneutcon(i)': '0',
                'cgnd(i)': '0',
                're0(r)': '0',
                'xe0(r)': '0',
                'ntrcn(i)': '0',
                'GPSlat(r)': gps_lat,
                'GPSlon(r)': gps_lon,
                'i_cont(i)': '0',
                'ilcph(i)': '0',
                'usetp(r)': get_usetp_regulator(types_table=types_table, value=comparator_value),
                'usp_up(r)': '1.11',
                'usp_low(r)': '0.9',
                'Tctrl(r)': '0.5',
                'ildc(r)': '0'
            }
            table.loc[index] = new_entry
            print(
                '----------------- Registro en ElmVoltreg creado con exito para:', loc_name, flush=True)
            return True
        elif tipo_fila_padre == constants.TYPTR2:
            volt_padre = fila_padre[0][15]
            voltaje_padre = re.search(r"\d+(\.\d+)?", volt_padre)
            voltaje_padre = voltaje_padre.group(0)
            voltaje_padre = calc.multiply_by_root3(float(voltaje_padre))
            modify_typ_voltreg(
                voltage=float(voltaje_padre) * float(rated_voltage),
                types_table=types_table,
                value=comparator_value)
            new_entry = {
                'ID(a:40)': element_id,
                'loc_name(a:40)': loc_name,
                'fold_id(p)': id_elm_net,
                'outserv(i)': '0',
                'typ_id(p)': get_type(
                    types_table=types_table,
                    value=comparator_value),
                'maxload(r)': '100',
                'nntap1(i)': '0',
                'lossAssign(i)': '0',
                'i_pcReglim(i)': '0',
                'ratfac(r)': '1',
                'cneutcon(i)': '0',
                'cgnd(i)': '0',
                're0(r)': '0',
                'xe0(r)': '0',
                'ntrcn(i)': '0',
                'GPSlat(r)': gps_lat,
                'GPSlon(r)': gps_lon,
                'i_cont(i)': '0',
                'ilcph(i)': '0',
                'usetp(r)': get_usetp_regulator(types_table=types_table, value=comparator_value),
                'usp_up(r)': '1.11',
                'usp_low(r)': '0.9',
                'Tctrl(r)': '0.5',
                'ildc(r)': '0'
            }
            table.loc[index] = new_entry
            print(
                '----------------- Registro en ElmVoltreg creado con exito para:', loc_name, flush=True)
            return True
        else:
            print(
                '=================================\nERROR! en metodo crear_elm_voltreg: No se encontro ningun tipo de elemento padre aceptable para el regulador',
                loc_name, '\nPor favor verificar archivo STD',
                '\nTipo de elemento padre:', tipo_fila_padre,
                '\nFila de elemento padre:', fila_padre, '\n'
                '=================================', flush=True)
            exit(21)
    else:
        print(
            '================================= \nERROR! en metodo crear_elm_voltreg: No se encontro el elemento padre para:',
            loc_name,
            '\n=================================', flush=True)
        exit(22)


def create_elm_lne(
        element_id,
        loc_name,
        id_elm_net,
        comparator_value,
        nlnph,
        dline,
        gps_coords_row,
        gps_coords_col,
        in_air,
        nneutral,
        uline,
        index,
        types_table,
        table):
    element_up = 0
    if not typ_lne_exists(types_table=types_table, value=comparator_value, uline=uline, nlnph=nlnph):
        if in_air == '1':
            tipo = constants.TYPLNE_AEREA
        else:
            tipo = constants.TYPLNE_SUB
        create_typ_lne(
            types_table=types_table,
            loc_name=comparator_value,
            uline=uline,
            nneutral=nneutral,
            nlnph=nlnph,
            tipo=tipo,
            element_id=element_id,
            fila_modelo=get_typ_lne_row(types_table=types_table, loc_name=comparator_value))
        element_up += 1
    new_entry = {
        'ID(a:40)': element_id + element_up,
        'loc_name(a:40)': loc_name,
        'fold_id(p)': id_elm_net,
        'typ_id(p)': get_typ_lne_id(types_table=types_table, value=comparator_value, uline=uline, nlnph=nlnph),
        'dline(r)': dline,
        'fline(r)': '1',
        'GPScoords:SIZEROW(i)': gps_coords_row,
        'GPScoords:SIZECOL(i)': gps_coords_col,
        'nlnum(i)': '1',
        'inAir(i)': in_air,
        'maxload(r)': '100',
        'Top(r)': '25',
        'lossAssign(i)': '0'}
    table.loc[index] = new_entry
    element_up += 1
    print('----------------- Registro en ElmLne creado con exito para:', loc_name, flush=True)
    return element_up


def create_typ_lne(
        types_table,
        loc_name,
        uline,
        nneutral,
        nlnph,
        element_id,
        tipo,
        fila_modelo):
    types_translator.create_typ_lne(element_id=element_id, loc_name=loc_name,
                                    iNomAir_sline=fila_modelo['InomAir(r)'].values[0],
                                    mlei=fila_modelo['mlei(a:2)'].values[0], rline=fila_modelo['rline(r)'].values[0],
                                    tipo=tipo,
                                    index=int(
                                        types_table['ID(a:40)'][types_table.index[-1]]) + 1,
                                    table=types_table)
    modify_typ_lne(types_table=types_table, typ_id=element_id,
                   uline=uline, nlnph=nlnph, nneutral=nneutral)
    return


def modify_typ_lne(
        types_table,
        typ_id,
        uline,
        nneutral,
        nlnph):
    types_table.loc[types_table['ID(a:40)']
                    == typ_id, 'uline(r)'] = uline
    types_table.loc[types_table['ID(a:40)']
                    == typ_id, 'nneutral(i)'] = nneutral
    types_table.loc[types_table['ID(a:40)']
                    == typ_id, 'nlnph(i)'] = nlnph
    return


def create_elm_term(
        element_id,
        loc_name,
        id_elm_net,
        phtech,
        root_table,
        root_name,
        root_type,
        node_table,
        node_name,
        gps_lat,
        gps_lon,
        index,
        table,
        root_types_table,
        terminal_root_id,
        root_row,
        element_type,
        root_type_for_insert):
    print('----------------- Creando terminal para elemento: ',
          root_name, ' Nombre del hijo: ', node_name, flush=True)
    root_id = get_type(types_table=root_table, value=root_name)
    node_id = get_type(types_table=node_table, value=node_name)
    root_voltage = constants.DEFAULT_EMPTY
    if not str(root_type) in constants.ELM_COUP_GROUP and str(
            root_type) != constants.ELM_XNET:
        root_voltage = get_root_voltage(
            root_id=root_id,
            root_table=root_table,
            root_type=str(root_type),
            types_table=root_types_table)
    else:
        if str(root_type) in constants.ELM_COUP_GROUP:
            root_voltage = constants.DEFAULT_EMPTY
        elif str(root_type) == constants.ELM_XNET:
            root_voltage = root_row['Q'].values[0]
    if is_of_type(types_table=table, value=loc_name):
        add_child_to_terminal(
            id_term=get_type(
                types_table=table,
                value=loc_name),
            node_id=node_id,
            table=table,
            element_type=element_type)
        return False
    else:
        if str(root_type) in constants.ELM_VOLTREG:
            print('Values to divide for:',
                  root_row['Q'].values[0], root_row['R'].values[0], root_row['S'].values[0], flush=True)
            root_voltage = float(root_voltage) / float(calc.get_comparator_value(
                [root_row['Q'].values[0], root_row['R'].values[0], root_row['S'].values[0]]))
        new_entry = {
            'ID(a:40)': element_id,
            'loc_name(a:40)': loc_name,
            'fold_id(p)': id_elm_net,
            'systype(i)': '0',
            'iUsage(i)': '1',
            'uknom(r)': str(root_voltage),
            'outserv(i)': '0',
            'vtarget(r)': '1',
            'vmax(r)': '1.05',
            'dvmax(r)': '5',
            'dvmin(r)': '-5',
            'vmin(r)': '0',
            'phtech(i)': phtech,
            'iEarth()': '0',
            'ivpriority(i)': -1,
            'RootID': terminal_root_id,
            'Nodes': 0,
            'NodesIDs': '',
            'RootType': str(root_type_for_insert),
            'NodesTypes': '',
            'GPSlat(r)': gps_lat,
            'GPSlon(r)': gps_lon}
        table.loc[index] = new_entry
        add_child_to_terminal(
            id_term=get_type(
                types_table=table,
                value=loc_name),
            node_id=node_id,
            table=table,
            element_type=element_type)
        print('----------------- Registro en ElmTerm creado con exito para:', loc_name, flush=True)
        return True


def add_child_to_terminal(id_term, node_id, table, element_type):
    num_nodes = table.loc[table['ID(a:40)'] == id_term, 'Nodes'].values[0]
    num_nodes += 1
    table.loc[table['ID(a:40)'] == id_term, 'Nodes'] = num_nodes
    node_ids = table.loc[table['ID(a:40)'] == id_term, 'NodesIDs'].values[0]
    node_ids += str(node_id) + ","
    table.loc[table['ID(a:40)'] == id_term, 'NodesIDs'] = node_ids
    nodes_types = table.loc[table['ID(a:40)']
                            == id_term, 'NodesTypes'].values[0]
    nodes_types += str(element_type) + ","
    table.loc[table['ID(a:40)'] == id_term, 'NodesTypes'] = nodes_types
    return


def create_sta_cubic(element_id, elm_term_table, term_index, table, elm_tr2_table):
    num_term = 0
    for index, row in elm_term_table.iterrows():
        terminal_id = row['ID(a:40)']
        root_id = row['RootID']
        root_type = row['RootType']
        node_ids = row['NodesIDs'].split(",")
        node_types = row['NodesTypes'].split(",")
        # Primero ingresamos el valor del nodo padre
        new_entry = {
            'ID(a:40)': element_id,
            'loc_name(a:40)': 'cub_1',
            'fold_id(p)': terminal_id,
            'obj_bus(i)': calc.get_obj_bus(value=root_type, default='null', obj_id=root_id, is_root=True, elm_tr2_table=elm_tr2_table),
            'obj_id(p)': root_id,
            'it2p1(i)': '0',
            'it2p2(i)': '1',
            'it2p3(i)': '2'
        }
        table.loc[term_index] = new_entry
        element_id += 1
        num_term += 1
        term_index += 1
        node_extension = 2
        pos = 0
        print('----------------- Registro en StaCubic creado con exito para elemento con ID:', root_id, flush=True)
        for node in node_ids:
            if node != '':
                new_entry = {
                    'ID(a:40)': element_id,
                    'loc_name(a:40)': 'cub_' + str(node_extension),
                    'fold_id(p)': terminal_id,
                    'obj_bus(i)': calc.get_obj_bus(value=node_types[pos], default='null', obj_id=node.rstrip(),
                                                   is_root=False, elm_tr2_table=elm_tr2_table),
                    'obj_id(p)': node,
                    'it2p1(i)': '0',
                    'it2p2(i)': '1',
                    'it2p3(i)': '2'}
                table.loc[term_index] = new_entry
                element_id += 1
                num_term += 1
                term_index += 1
                node_extension += 1
                pos += 1
            print(
                '----------------- Registro en StaCubic creado con exito para nodos del elemento con ID:', root_id, flush=True)
    return num_term
