import app_constants as constants

from utilities import calculation_helper as calc
from utilities import ids
from utilities import elements_translator
from utilities import graphics_translator
from utilities import lowest_coords
from utilities.create_terminals import get_element_table_for

def create_graphic_tables(std, rel_fuse_table, int_grf_table, elm_term_table, elm_coup_table, int_grf_con_table, element_tables):
  """
  Crea todas las tablas que corresponden a la ubicacion de la red
  en el software de Powerfactory.
  Tablas creadas: IntGrf, IntGrfcon.
  :return:
  """
  print('************************************************************  INICIA CREACION DE TABLA: IntGrf ************************************************************', flush=True)
  for index, row in std.iterrows():
    print('----------------- Creando registro en tabla IntGrf para elemento tipo:', row['B'], flush=True)
    if row['B'] == constants.TYPFUSE:
      if elements_translator.is_of_type(types_table=rel_fuse_table, value=row['A']):
        elm_created = graphics_translator.create_int_grf(element_id=ids.global_id,
                                                          loc_name=row['A'],
                                                          id_int_grf_net=ids.int_grf_net_id,
                                                          coord_x=row['F'], 
                                                          coord_y=row['G'],
                                                          tipo=str(row['B']),
                                                          elements_table=get_element_table_for(tipo=row['B'], tables=element_tables),
                                                          comparator_value=row['A'],
                                                          rot=0,
                                                          table=int_grf_table, 
                                                          index=ids.int_grf_id,
                                                          term_table=elm_term_table,
                                                          elmcoup_data=elm_coup_table,
                                                          lowest_center=[lowest_coords.x, lowest_coords.y])
        ids.int_grf_id += elm_created
        ids.global_id += elm_created
    else:
      elm_created = graphics_translator.create_int_grf(element_id=ids.global_id,
                                                        loc_name=row['A'],
                                                        id_int_grf_net=ids.int_grf_net_id,
                                                        coord_x=row['F'],
                                                        coord_y=row['G'],
                                                        tipo=str(row['B']),
                                                        elements_table=get_element_table_for(tipo=row['B'], tables=element_tables),
                                                        comparator_value=row['A'],
                                                        rot=0,
                                                        table=int_grf_table,
                                                        index=ids.int_grf_id,
                                                        term_table=elm_term_table,
                                                        elmcoup_data=elm_coup_table,
                                                        lowest_center=[lowest_coords.x, lowest_coords.y])
      ids.int_grf_id += elm_created
      ids.global_id += elm_created

  print('****************************************  INICIA CREACION DE TABLA: IntGrfcon ****************************************', flush=True)
  for index, row in int_grf_table.iterrows():
    if row['sSymNam(a:40)'] != 'PointTerm':
      print('----------------- Creando registro en tabla IntGrfcon para elemento tipo: ',
            row['sSymNam(a:40)'], flush=True)
      registros = graphics_translator.create_int_grf_con(element_id=ids.global_id,
                                                      id_en_int_grf=row['ID(a:40)'],
                                                      table=int_grf_con_table,
                                                      term_table=elm_term_table,
                                                      int_grf_table=int_grf_table,
                                                      p_data_obj=row['pDataObj(p)'],
                                                      tipo=calc.get_type_from_sys_name(sys_name=row['sSymNam(a:40)']),
                                                      index=ids.int_grfcon_id,
                                                      center=[float(row['rCenterX(r)']), float(row['rCenterX(r)'])],
                                                      rot=0)

    ids.int_grfcon_id += registros
    ids.global_id += registros
  return
