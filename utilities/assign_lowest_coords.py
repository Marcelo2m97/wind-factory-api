import app_constants as constants

from utilities import lowest_coords
from utilities import calculation_helper as calc

def assign_lowest_coords(std_file):
    coords = calc.get_lowest_coord(std_file=std_file)
    lowest_coords.x = coords[0] - (6 * constants.UM_PF)
    lowest_coords.y = coords[1] - (6 * constants.UM_PF)
    return
