import app_constants as constants

def typ_lne_exists(types_table, value, uline, nlnph):
    table_name = types_table.loc[types_table['loc_name(a:40)'] == value]
    volt_table = table_name.loc[table_name['uline(r)'] == uline]
    phases_table = volt_table.loc[volt_table['nlnph(i)'] == nlnph]
    return phases_table.loc[phases_table['loc_name(a:40)']
                            == value, 'ID(a:40)'].values > 0


def get_typ_lne_id(types_table, value, uline, nlnph):
    table_name = types_table.loc[types_table['loc_name(a:40)'] == value]
    volt_table = table_name.loc[table_name['uline(r)'] == uline]
    phases_table = volt_table.loc[volt_table['nlnph(i)'] == nlnph]
    return phases_table.loc[phases_table['loc_name(a:40)']
                            == value, 'ID(a:40)'].values[0]


def get_typ_lne_row(types_table, loc_name):
    table_name = types_table.loc[types_table['loc_name(a:40)'] == loc_name]
    volt_table = table_name.loc[table_name['uline(r)']
                                == constants.DEFAULT_EMPTY]
    phases_table = volt_table.loc[volt_table['nlnph(i)']
                                  == constants.DEFAULT_EMPTY]
    print(phases_table.loc[phases_table['loc_name(a:40)']
          == loc_name, 'ID(a:40)'].values, flush=True)
    if len(phases_table.loc[phases_table['loc_name(a:40)'] == loc_name, 'ID(a:40)'].values) > 0:
        return phases_table.loc[phases_table['loc_name(a:40)'] == loc_name]
    else:
        print('No se encontro una fila modelo para crear el tipo linea: \nTabla de tipos:', types_table,
              '\nTabla filtrada por nombre ', loc_name, ':', table_name, flush=True)
        print('Fila modelo para el tipo linea a regresar:',
              phases_table.loc[phases_table['loc_name(a:40)'] == loc_name], flush=True)
        exit(29)
