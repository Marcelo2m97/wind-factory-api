import app_constants as constants

from utilities.get_value_from_table import get_value_from_table

def create_int_grf_4_terminal(element_id, root_id, center_x, center_y, rot, root_type, term_table,
                             id_int_grf_net, index, table):
    """
    Crea el registro en IntGrf para la terminal de un elemento.
    :param element_id: ID que tendra la terminal dentro de IntGrf.
    :param root_id: ID del elemento padre.
    :param center_x: Coordenada en X del centro de la terminal.
    :param center_y: Coordenada en Y del centro de la terminal
    :param rot: Rotacion del elemento.
    :param root_type: Tipo de elemento padre.
    :param term_table: Tabla correspondiente a todos los terminales.
    :param id_int_grf_net: ID del registro de la tabla IntGrfNet.
    :param index: Posicion en la que se va a crear el registro dentro de su respectiva tabla.
    :param table: Tabla en la que se creara el registro.
    :return: True si se creo un registro False en caso contrario.
    """
    loc_name = get_value_from_table(value=root_id, table=term_table, comparator_column_name='RootID',
                                      result_column_name='loc_name(a:40)', stop_on_error=False)
    terminal_id = get_value_from_table(value=root_id, table=term_table, comparator_column_name='RootID',
                                         result_column_name='ID(a:40)', stop_on_error=False)
    sys_name = 'PointTerm'

    center_coord = get_terminal_coords(root_type=root_type, root_center=[
                                       center_x, center_y], rot=rot)
    if center_coord[0] != -1 and center_coord[1] != -1 and isinstance(terminal_id, int):
        new_entry = {
          'ID(a:40)': element_id,
          'loc_name(a:40)': 'GraphNetObj_' + str(loc_name),
          'fold_id(p)': id_int_grf_net,
          'rCenterX(r)': center_coord[0],
          'rCenterY(r)': center_coord[1],
          'sSymNam(a:40)': sys_name,
          'pDataObj(p)': terminal_id,
          'iRot(i)': rot,
          'rSizeX(r)': 1,
          'rSizeY(r)': 1
        }
        table.loc[index] = new_entry
        return True
    else:
        return False


def get_terminal_coords(root_type, root_center, rot):
    """
    Obtiene las coordenadas de la terminal de un elemeto padre dependiendo de su tipo y rotacion.
    :param root_type: Tipo del elemento padre.
    :param root_center: Centro del elemento padre [x, y].
    :param rot: Rotacion del elemento.
    :return: Coordenadas de la terminal.
    """
    if root_type == constants.ELM_XNET:
        return get_elmXNet_terminal_coords(rot=rot, root_center=root_center)
    elif root_type in constants.ELM_COUP_GROUP:
        return get_elm_coup_terminal_coords(rot=rot, root_center=root_center)
    elif root_type == constants.ELM_LNE_AIR or root_type == constants.ELM_LNE_SUB:
        return get_elm_lne_terminal_coords(rot=rot, root_center=root_center)
    elif root_type == constants.TYPFUSE:
        return get_rel_fuse_terminal_coords(rot=rot, root_center=root_center)
    elif root_type == constants.TYPTR2:
        return get_elm_tr2_terminal_coords(rot=rot, root_center=root_center)
    elif root_type == constants.ELM_VOLTREG:
        return get_elm_voltreg_terminal_coords(rot=rot, root_center=root_center)
    return [-1, -1]


def get_elmXNet_terminal_coords(rot, root_center):
    """
    Obtiene las coordenadas de la terminal de unelemento tipo fuente.
    :param rot: Rotacion del elemento padre.
    :param root_center: Centro del elemento padre.
    :return: Coordenadas de la terminal.
    """
    x1 = -1
    y1 = -1
    if rot == 0:
        x1 = root_center[0]
        y1 = float(root_center[1]) + (3 * constants.UM_PF)
    if rot == 90:
        x1 = float(root_center[0]) - constants.UM_PF
        y1 = root_center[1]
    if rot == 180:
        x1 = root_center[0]
        y1 = float(root_center[1]) - (3 * constants.UM_PF)
    if rot == 270:
        x1 = float(root_center[0]) + constants.UM_PF
        y1 = root_center[1]
    return [x1, y1]


def get_elm_coup_terminal_coords(rot, root_center):
    """
    Obtiene las coordenadas de la terminal de un elemento tipo coup.
    :param rot: Rotacion del elemento padre.
    :param root_center: Centro del elemento padre.
    :return: Coordenadas de la terminal.
    """
    x1 = -1
    y1 = -1
    if rot == 0:
        x1 = root_center[0]
        y1 = float(root_center[1]) + (3 * constants.UM_PF)
    if rot == 90:
        x1 = float(root_center[0]) - (3 * constants.UM_PF)
        y1 = root_center[1]
    if rot == 180:
        x1 = root_center[0]
        y1 = float(root_center[1]) - (3 * constants.UM_PF)
    if rot == 270:
        x1 = float(root_center[0]) + (3 * constants.UM_PF)
        y1 = root_center[1]
    return [x1, y1]


def get_elm_lne_terminal_coords(rot, root_center):
    """
    Obtiene las coordenadas de la terminal de un elemento tipo linea.
    :param rot: Rotacion del elemento padre.
    :param root_center: Centro del elemento padre.
    :return: Coordenadas de la terminal.
    """
    x1 = -1
    y1 = -1
    if rot == 0:
        x1 = root_center[0]
        y1 = float(root_center[1]) - (2 * constants.UM_PF)
    if rot == 90:
        x1 = float(root_center[0]) + (2 * constants.UM_PF)
        y1 = root_center[1]
    if rot == 180:
        x1 = root_center[0]
        y1 = float(root_center[1]) + (2 * constants.UM_PF)
    if rot == 270:
        x1 = float(root_center[0]) - (2 * constants.UM_PF)
        y1 = root_center[1]
    return [x1, y1]


def get_rel_fuse_terminal_coords(rot, root_center):
    """
    Obtiene las coordenadas de la terminal de un elemento tipo fusible.
    :param rot: Rotacion del elemento padre.
    :param root_center: Centro del elemento padre.
    :return: Coordenadas de la terminal.
    """
    x1 = -1
    y1 = -1
    if rot == 0:
        x1 = root_center[0]
        y1 = float(root_center[1]) - (3.5 * constants.UM_PF)
    if rot == 90:
        x1 = float(root_center[0]) + (3.5 * constants.UM_PF)
        y1 = root_center[1]
    if rot == 180:
        x1 = root_center[0]
        y1 = float(root_center[1]) + (3.5 * constants.UM_PF)
    if rot == 270:
        x1 = float(root_center[0]) - (3.5 * constants.UM_PF)
        y1 = root_center[1]
    return [x1, y1]


def get_elm_tr2_terminal_coords(rot, root_center):
    """
    Obtiene las coordenadas de la terminal de un elemento tipo transformador.
    :param rot: Rotacion del elemento padre.
    :param root_center: Centro del elemento padre.
    :return: Coordenadas de la terminal.
    """
    x1 = -1
    y1 = -1
    if rot == 0:
        x1 = root_center[0]
        y1 = float(root_center[1]) - (4 * constants.UM_PF)
    if rot == 90:
        x1 = float(root_center[0]) + (4 * constants.UM_PF)
        y1 = root_center[1]
    if rot == 180:
        x1 = root_center[0]
        y1 = float(root_center[1]) + (4 * constants.UM_PF)
    if rot == 270:
        x1 = float(root_center[0]) - (4 * constants.UM_PF)
        y1 = root_center[1]
    return [x1, y1]


def get_elm_voltreg_terminal_coords(rot, root_center):
    """
    Obtiene las coordenadas de la terminal de un elemento tipo regulador.
    :param rot: Rotacion del elemento padre.
    :param root_center: Centro del elemento padre.
    :return: Coordenadas de la terminal.
    """
    x1 = -1
    y1 = -1
    if rot == 0:
        x1 = root_center[0]
        y1 = float(root_center[1]) - (3.5 * constants.UM_PF)
    if rot == 90:
        x1 = float(root_center[0]) + (3.5 * constants.UM_PF)
        y1 = root_center[1]
    if rot == 180:
        x1 = root_center[0]
        y1 = float(root_center[1]) + (3.5 * constants.UM_PF)
    if rot == 270:
        x1 = float(root_center[0]) - (3.5 * constants.UM_PF)
        y1 = root_center[1]
    return [x1, y1]
