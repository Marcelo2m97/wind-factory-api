import app_constants as constants

def typ_tr2_exists(types_table, loc_name, nt2ph, utrn_l, utrn_h):
    filtered_by_name = types_table.loc[types_table['loc_name(a:40)']
                                       == loc_name]
    filtered_by_phases = filtered_by_name.loc[filtered_by_name['nt2ph(i)']
                                              == nt2ph]
    filtered_by_volt_l = filtered_by_phases.loc[
        filtered_by_phases['utrn_l(r)'] == utrn_l]
    return filtered_by_volt_l.loc[filtered_by_volt_l['utrn_h(r)']
                                  == utrn_h, 'ID(a:40)'].values > 0


def get_typ_tr2_id(types_table, loc_name, nt2ph, utrn_l, utrn_h):
    filtered_by_name = types_table.loc[types_table['loc_name(a:40)']
                                       == loc_name]
    filtered_by_phases = filtered_by_name.loc[filtered_by_name['nt2ph(i)']
                                              == nt2ph]
    filtered_by_volt_l = filtered_by_phases.loc[
        filtered_by_phases['utrn_l(r)'] == utrn_l]
    return filtered_by_volt_l.loc[filtered_by_volt_l['utrn_h(r)']
                                  == utrn_h, 'ID(a:40)'].values[0]


def get_typ_tr2_row(types_table, loc_name):
    filtered_by_name = types_table.loc[types_table['loc_name(a:40)']
                                       == loc_name]
    filtered_by_phases = filtered_by_name.loc[filtered_by_name['nt2ph(i)']
                                              == constants.DEFAULT_EMPTY]
    filtered_by_volt_l = filtered_by_phases.loc[
        filtered_by_phases['utrn_l(r)'] == constants.DEFAULT_EMPTY]
    return filtered_by_volt_l.loc[filtered_by_volt_l['utrn_h(r)']
                                  == constants.DEFAULT_EMPTY]


def modify_typ_tr2(
        typ,
        typ_id,
        nt2ph,
        utrn_h,
        utrn_l,
        tr2cn_h,
        tr2cn_l):
    strn_value = float(typ.loc[typ['ID(a:40)'] == typ_id, 'strn(r)'])
    typ.loc[typ['ID(a:40)'] == typ_id, 'nt2ph(i)'] = nt2ph
    typ.loc[typ['ID(a:40)'] == typ_id, 'strn(r)'] = str(
        strn_value * float(nt2ph) / 1000)
    typ.loc[typ['ID(a:40)'] == typ_id, 'utrn_h(r)'] = utrn_h
    typ.loc[typ['ID(a:40)'] == typ_id, 'utrn_l(r)'] = utrn_l
    typ.loc[typ['ID(a:40)'] == typ_id, 'tr2cn_h(a:2)'] = tr2cn_h
    typ.loc[typ['ID(a:40)'] == typ_id, 'tr2cn_l(a:2)'] = tr2cn_l
    return
