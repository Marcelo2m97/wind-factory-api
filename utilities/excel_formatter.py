def create_headers_seq():
    return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U',
            'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC']


def create_headers_std():
    return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ']


def drop_columns(desde, hasta):
    columnas = []
    for x in range(desde, hasta):
        columnas.append(x)
    return columnas


def create_headers_typ_lne():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'uline(r)',
        'sline(r)',
        'InomAir(r)',
        'cohl_(i)',
        'rline(r)',
        'xline(r)',
        'rline0(r)',
        'xline0(r)',
        'tmax(r)',
        'systp(i)',
        'nlnph(i)',
        'nneutral(i)',
        'frnom(r)',
        'mlei(a:2)',
        'bline(r)',
        'bline0(r)',
        'rnline(r)',
        'rpnline(r)',
        'xnline(r)',
        'xpnline(r)',
        'tline0(r)',
        'tline(r)',
        'bnline(r)',
        'bpnline(r)'
    ]


def create_headers_typ_fuse():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'itype(i)',
        'urat(r)',
        'irat(r)',
        'frq(r)',
        'chartype(i)'
    ]


def create_headers_typ_tr2():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'nt2ph(i)',
        'strn(r)',
        'frnom(r)',
        'utrn_h(r)',
        'utrn_l(r)',
        'uktr(r)',
        'pcutr(r)',
        'uk0tr(r)',
        'ur0tr(r)',
        'tr2cn_h(a:2)',
        'tr2cn_l(a:2)',
        'nt2ag(r)',
        'curmg(r)',
        'pfe(r)',
        'zx0hl_n(r)',
        'itapch(i)',
        'rtox0_n(r)',
        'itrdl(r)',
        'itrdl_lv(r)',
        'itrdr(r)',
        'itrdr_lv(r)',
        'tapchtype(i)',
        'tap_side(i)',
        'dutap(r)',
        'phitr(r)',
        'nntap0(i)',
        'ntpmn(i)',
        'ntpmx(i)',
        'itapzdep(i)',
        'itapch2(i)',
        'itrldf(i)',
        'itratioadpt(i)'
    ]


def create_headers_typ_voltreg():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'type(i)',
        'config(i)',
        'frnom(r)',
        'Un(r)',
        'In(r)',
        'pcReg(r)',
        'nntap0(i)',
        'ntapmin(i)',
        'ntapmax(i)',
        'uk(r)',
        'pcu(r)',
        'itapzdep(i)',
        'usetp(r)'
    ]


def create_headers_typ_lod():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'systp(i)',
        'phtech(i)',
        'aP(r)',
        'bP(r)',
        'kpu0(r)',
        'kpu1(r)',
        'kpu(r)',
        'aQ(r)',
        'bQ(r)',
        'kqu0(r)',
        'kqu1(r)',
        'kqu(r)'
    ]


def create_headers_typ_switch():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'Inom(r)',
        't_open(r)',
        'R_on(r)',
        'X_on(r)'
    ]


def create_headers_gral():
    return [
        'ID(a:40)',
        'Descr(a:40)',
        'Val(a:40)'
    ]


def create_headers_elm_net():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'frnom(r)',
        'pDiagram(p)'
    ]


def create_headers_int_grf_net():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'snap_on(i)',
        'grid_on(i)',
        'ortho_on(i)',
        'pDataFolder(p)'
    ]


def create_headers_elm_shnt():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'systp(i)',
        'ctech(i)',
        'shtype(i)',
        'ushnm(r)',
        'mode_inp(a:3)',
        'qcapn(r)',
        'ncapx(i)',
        'ncapa(i)',
        'outserv(i)',
        'tandc(r)',
        'iswitch(i)',
        'iTaps(i)',
        'GPSlat(r)',
        'GPSlon(r)'
    ]


def create_headers_elmXnet():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'bustp(a:2)',
        'pgini(r)',
        'qgini(r)',
        'usetp(r)',
        'outserv(i)',
        'Re(r)',
        'Xe(r)',
        'uset_mode(i)',
        'mode_inp(a:3)',
        'iintgnd(i)',
        'cgnd(i)',
        'cpeter(i)',
        'Kpf(r)',
        'K(r)',
        'GPSlat(r)',
        'GPSlon(r)'
    ]


def create_headers_elmLod():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'typ_id(p)',
        'mode_inp(a:3)',
        'plini(r)',
        'qlini(r)',
        'scale0(r)',
        'i_scale(i)',
        'outserv(i)',
        'classif(a:20)',
        'i_sym(i)',
        'u0(r)',
        'iLoadTrf(i)',
        'GPSlat(r)',
        'GPSlon(r)'
    ]


def create_headers_elmTr2():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'typ_id(p)',
        'ntnum(i)',
        'outserv(i)',
        'nntap(i)',
        'iTaps(i)',
        'maxload(r)',
        'lossAssign(i)',
        'ratfac(r)',
        'cneutcon(i)',
        'cpeter_l(i)',
        'cgnd_l(i)',
        're0tr_h(r)',
        'xe0tr_h(r)',
        'GPSlat(r)',
        'GPSlon(r)',
        'IsReductor'
    ]


def create_headers_relFuse():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'typ_id(p)',
        'on_off(i)',
        'aUsage(a:4)',
        'outserv(i)',
        'nphase(i)',
        'nneutral(i)',
        'iphauto(i)',
        'calcuse(i)',
        'dev_no(i)',
        'GPSlon(r)',
        'GPSlat(r)'
    ]


def create_headers_elmCoup():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'typ_id(p)',
        'on_off(i)',
        'aUsage(a:4)',
        'nphase(i)',
        'nneutral(i)',
        'GPSlat(r)',
        'GPSlon(r)'
    ]


def create_headers_elmVoltreg():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'outserv(i)',
        'typ_id(p)',
        'maxload(r)',
        'nntap1(i)',
        'lossAssign(i)',
        'i_pcReglim(i)',
        'ratfac(r)',
        'cneutcon(i)',
        'cgnd(i)',
        're0(r)',
        'xe0(r)',
        'ntrcn(i)',
        'GPSlat(r)',
        'GPSlon(r)',
        'i_cont(i)',
        'ilcph(i)',
        'usetp(r)',
        'usp_up(r)',
        'usp_low(r)',
        'Tctrl(r)',
        'ildc(r)'
    ]


def create_headers_elmLne():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'typ_id(p)',
        'dline(r)',
        'fline(r)',
        'GPScoords:SIZEROW(i)',
        'GPScoords:SIZECOL(i)',
        'nlnum(i)',
        'inAir(i)',
        'maxload(r)',
        'Top(r)',
        'lossAssign(i)'
    ]


def create_headers_elmTerm():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'systype(i)',
        'iUsage(i)',
        'uknom(r)',
        'outserv(i)',
        'vtarget(r)',
        'vmax(r)',
        'dvmax(r)',
        'dvmin(r)',
        'vmin(r)',
        'phtech(i)',
        'iEarth()',
        'ivpriority(i)',
        'RootID',
        'Nodes',
        'NodesIDs',
        'RootType',
        'NodesTypes',
        'GPSlat(r)',
        'GPSlon(r)'
    ]


def create_headers_staCubic():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'obj_bus(i)',
        'obj_id(p)',
        'it2p1(i)',
        'it2p2(i)',
        'it2p3(i)'
    ]


def create_headers_intGrf():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'rCenterX(r)',
        'rCenterY(r)',
        'sSymNam(a:40)',
        'pDataObj(p)',
        'iRot(i)',
        'rSizeX(r)',
        'rSizeY(r)']


def create_headers_intGrfcon():
    return [
        'ID(a:40)',
        'loc_name(a:40)',
        'fold_id(p)',
        'rX:SIZEROW(i)',
        'rX:0(r)',
        'rX:1(r)',
        'rX:2(r)',
        'rY:SIZEROW(i)',
        'rY:0(r)',
        'rY:1(r)',
        'rY:2(r)']

