def get_usetp_regulator(types_table, value):
    return types_table.loc[types_table['loc_name(a:40)'] == value, 'usetp(r)'].values[0]
