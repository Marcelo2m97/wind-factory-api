import app_constants as constants

from utilities import calculation_helper as calc
from utilities import ids
from utilities import elements_translator

def create_terminals(tables, std, types_table):
        """
        Funcion: Esta funcion recorre el archivo STD y por cada elemento
        (a excepcion de los registros que tengan ROOT en su columna C) crea una terminal
        para su respectivo padre y asigna el elemento como nodo hijo del padre.
        Proceso:
        1. Obtener fila del padre
        2. Obtener tabla a la que pertenece el nodo (fila) que estamos analizando.
        3. Si el valor de 'tipo' (columna B) para el padre es 6 o 10, se obtiene su padre hasta que se encuentre
            un elemento que no sea tipo 6.
        4. Llamamos la funcion de crear elemento terminal y le pasamos como parametros:
            Tabla a la que pertenece el elemento padre.
            Tipo del elemento padre.
            Tabla de los tipos del elemento padre.
            Nombre del elemento padre.
            Tabla a la que pertenece el elemento hijo.
            Nombre del elemento hijo.
        5. Se aumenta el ID global y el ID de ElmTerm.
        :return:
        """
        for index, row in std.iterrows():
            # Se ignora la primera linea y todas aquellas que tengan como padre
            # a ROOT
            if str(row['D']) != 'ROOT' and index > 0:
                #  Obtenemos la fila correspondiente al padre del nodo.
                root_row = std.loc[std['A'] == row['D']]
                # Obtenemos las fases del elemento padre de la terminal
                phases = root_row['C'].values[0]
                #  Obtenemos la tabla de elementos que corresponde al nodo.
                node_table = get_element_table_for(tipo=row['B'], tables=tables)
                terminal_root_id = elements_translator.get_type(
                    types_table=get_element_table_for(
                        tipo=root_row['B'].values[0], tables=tables), value=root_row['A'].values[0])
                #  Si el tipo de elemento del padre pertenece a ELMCOUP
                #  Quiere decir que debemos asignarle a la terminal el valor de voltaje
                #  del elemento anterior.
                root_type = root_row['B'].values[0]
                while str(root_row['B'].values[0]) in constants.ELM_COUP_GROUP:
                    #  Modificamos la fila padre a ser la fila padre del padre hasta que encontremos una fila
                    #  que no sea elmcoup.
                    root_row = std.loc[std['A']
                                            == root_row['D'].values[0]]

                # Creamos el valor de la terminal utilizando el nombre del
                # padre + _terminal
                if elements_translator.create_elm_term(
                        element_id=ids.global_id,
                        loc_name=str(
                            row['D']) + '_Term',
                        id_elm_net=ids.elm_net_id,
                        phtech=calc.convert_terminal_phase_tech(phases),
                        root_table=get_element_table_for(
                            tipo=root_row['B'].values[0], tables=tables),
                        root_name=root_row['A'].values[0],
                        node_name=row['A'],
                        node_table=node_table,
                        gps_lat=row['H'],
                        gps_lon=row['I'],
                        index=ids.elm_term_id,
                        table=tables[8],
                        root_types_table=get_types_table(
                            tipo=root_row['B'].values[0],
                            grupo=root_row['C'].values[0],
                            types_table=types_table),
                        root_type=root_row['B'].values[0],
                        terminal_root_id=terminal_root_id,
                        root_row=root_row,
                        element_type=row['B'],
                        root_type_for_insert=root_type):
                    ids.global_id += 1
                    ids.elm_term_id += 1
                    is_first = False
        assign_son_voltages(tables)
        return


def get_element_table_for(tipo, tables):
  """
  Esta funcion nos permite obtener la lista de elementos a la cual pertenece este tipo.

  :param tipo: Tipo de elemento, Columna B proveniento del archivo STD.
  :return: Lista de todos los elementos registrados para ese tipo.
  """
  if str(tipo) == constants.ELM_LNE_AIR or str(
          tipo) == constants.ELM_LNE_SUB:
    return tables[7]
  elif str(tipo) == constants.ELM_SHNT:
    return tables[0]
  elif str(tipo) == constants.ELM_VOLTREG:
    return tables[6]
  elif str(tipo) == constants.TYPTR2:
    return tables[3]
  elif str(tipo) in constants.ELM_COUP_GROUP:
    return tables[5].append(tables[4])
  elif str(tipo) == constants.ELM_XNET:
    return tables[1]
  elif str(tipo) == constants.ELM_LOD:
    return tables[2]
  else:
    print(
        "No se encontro ninguna lista de elementos correspondiente al tipo ingresado: ",
        tipo, flush=True)
    exit(26)

def assign_son_voltages(tables):
    """
    Asigna el voltaje a todos los elementos hijos.

    Itera todas las filas que existan en la tabla ElmTerm y verifica el valor de
    'uknom' que representa el valor del voltaje, si encuentra un valor de
    voltaje nulo, se asigna el valor de su padre.
    :return:
    """
    for index, row in tables[8].iterrows():
        if str(row['uknom(r)']) == str(constants.DEFAULT_EMPTY):
            tables[8].loc[index, 'uknom(r)'] = get_first_upper_voltage(
                index=index, tables=tables)
    return

def get_first_upper_voltage(index, tables):
    """
    Obtiene el valor de voltaje del elemento padre.

    Busca en la columna uknom para el elemento en la posicion index dentro de la tabla de ElmTerm
    y retorna el valor si este es diferente de nulo.

    :param index: Indice del elemento en de la tabla ElmTerm.
    :return: El voltaje del elemento padre o nulo en caso de que asi sea.
    """
    while index > 0:
        if tables[8].loc[index,
                                'uknom(r)'] != constants.DEFAULT_EMPTY:
            print('Voltage found:',
                  tables[8].loc[index, 'uknom(r)'].values[0], flush=True)
            return tables[8].loc[index, 'uknom(r)'].values[0]
        index -= 1
    return constants.DEFAULT_EMPTY

def get_types_table(tipo, grupo, types_table):
    """
    Esta funcion nos permite obtener la lista de tipos para un elemento en especifico

    :param tipo: tipo de elemento proveniente del archivo STD columna B
    :param grupo: grupo al que pertenece el elemento, columna C archivo STD
    :return: Lista de todos los registros correspondientes a ese tipo.
    """
    if str(tipo) == constants.ELM_LNE_AIR or str(
            tipo) == constants.ELM_LNE_SUB:
        return types_table[0]
    elif str(tipo) == constants.ELM_SHNT:
        return str(constants.DEFAULT_EMPTY)
    elif str(tipo) == constants.ELM_VOLTREG:
        return types_table[3]
    elif str(tipo) == constants.TYPTR2:
        return types_table[2]
    elif str(tipo) in constants.ELM_COUP_GROUP:
        if str(tipo) == '10' and str(grupo) == '5':
            return types_table[1]
        else:
            return types_table[5]
    elif str(tipo) == constants.ELM_XNET:
        return str(constants.DEFAULT_EMPTY)
    elif str(tipo) == constants.ELM_LOD:
        print(
            'Se encontro un objeto consumidor como padre de una terminal, '
            'lo cual no puede ser posible. Por favor verificar la informacion del modelo de red (archivo std).', flush=True)
        exit(24)
        return str(constants.DEFAULT_EMPTY)
    else:
        print(
            "No se encontro ningun tipo para el elemento padre al momento de crear la terminal", flush=True)
        exit(23)
    return

