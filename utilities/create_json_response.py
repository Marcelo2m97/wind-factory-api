def create_json_response(typ_content, length):
    response = []
    elements_number = range(length)
    for index in elements_number:
        response.append(typ_content[str(index)])
    return response
