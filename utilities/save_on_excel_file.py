import pandas as pd

import app_constants as constants

from utilities.reset_ids import reset_ids


def save_on_excel_file(tables):
  print(
      '************************************************************ ESCRIBIENDO TABLAS A ARCHIVO FINAL:', constants.FINAL_EXCEL_FILENAME, ' ************************************************************', flush=True)
  writer = pd.ExcelWriter(constants.FINAL_EXCEL_FILENAME)
  print('Escribiendo hoja de General...', flush=True)
  tables[0].to_excel(writer, sheet_name="General", index=False)
  print('Escribiendo hoja de ElmNet...', flush=True)
  tables[1].to_excel(writer, sheet_name="ElmNet", index=False)
  print('Escribiendo hoja de IntGrf...', flush=True)
  tables[2].to_excel(writer, sheet_name="IntGrf", index=False)
  print('Escribiendo hoja de IntGrfcon...', flush=True)
  tables[3].to_excel(writer, sheet_name="IntGrfcon", index=False)
  print('Escribiendo hoja de IntGrfnet...', flush=True)
  tables[4].to_excel(writer, sheet_name="IntGrfnet", index=False)
  print('Escribiendo hoja de ElmShnt...', flush=True)
  tables[5].to_excel(writer, sheet_name="ElmShnt", index=False)
  print('Escribiendo hoja de ElmXnet...', flush=True)
  tables[6].to_excel(writer, sheet_name="ElmXnet", index=False)
  print('Escribiendo hoja de ElmLod...', flush=True)
  tables[7].to_excel(writer, sheet_name="ElmLod", index=False)
  print('Escribiendo hoja de ElmTr2...', flush=True)
  tables[8].to_excel(writer, sheet_name="ElmTr2", index=False)
  print('Escribiendo hoja de RelFuse...', flush=True)
  tables[9].to_excel(writer, sheet_name="RelFuse", index=False)
  print('Escribiendo hoja de ElmCoup...', flush=True)
  tables[10].to_excel(writer, sheet_name="ElmCoup", index=False)
  print('Escribiendo hoja de ElmVoltreg...', flush=True)
  tables[11].to_excel(writer, sheet_name="ElmVoltreg", index=False)
  print('Escribiendo hoja de ElmLne...', flush=True)
  tables[12].to_excel(writer, sheet_name="ElmLne", index=False)
  print('Escribiendo hoja de ElmTerm...', flush=True)
  tables[13].to_excel(writer, sheet_name="ElmTerm", index=False)
  print('Escribiendo hoja de StaCubic...', flush=True)
  tables[14].to_excel(writer, sheet_name="StaCubic", index=False)
  print('Escribiendo hoja de TypLne...', flush=True)
  tables[15].to_excel(writer, sheet_name="TypLne", index=False)
  print('Escribiendo hoja de TypFuse...', flush=True)
  tables[16].to_excel(writer, sheet_name="TypFuse", index=False)
  print('Escribiendo hoja de TypLod...', flush=True)
  tables[17].to_excel(writer, sheet_name="TypLod", index=False)
  print('Escribiendo hoja de TypTr2...', flush=True)
  tables[18].to_excel(writer, sheet_name="TypTr2", index=False)
  print('Escribiendo hoja de TypSwitch...', flush=True)
  tables[19].to_excel(writer, sheet_name="TypSwitch", index=False)
  print('Escribiendo hoja de TypVoltreg...', flush=True)
  tables[20].to_excel(writer, sheet_name="TypVoltreg", index=False)
  try:
    writer.save()  # Escribimos todas las hojas de excel declaradas
  except PermissionError:
    print(
          'Verificar que el archivo,',
          constants.FINAL_EXCEL_FILENAME,
          " no esta siendo utilizado.", flush=True)
    exit(26)
  print(
    'Excel creado con exito. \nNombre de archivo:',
    constants.FINAL_EXCEL_FILENAME, flush=True)
  reset_ids()
