import app_constants as constants


def create_typ_lne(
        element_id,
        loc_name,
        iNomAir_sline,
        mlei,
        rline,
        tipo,
        index,
        table):
    """
    Ingresa los valores como un registro nuevo en la tabla de TypLne
    :param element_id: id del elemento a ingresa.
    :param loc_name: Nombre del elemento.
    :param iNomAir_sline: Valor de iNomAir o sLine en Windmil
    :param mlei: Valor proveniente de Windmil.
    :param rline: Valor proveniente de Windmil.
    :param tipo: Valor proveniente de Windmil.
    :param index: Posicion del registro en su respectiva tabla.
    :param table: Tabla en la que se creara el registro.
    :return: N/A
    """
    # Valores por defecto
    sLine = '1'
    iNomAir = '1'
    sysTp = '0'
    # Valores obtenidos del elemento
    nlnph = constants.DEFAULT_EMPTY
    nneutral = constants.DEFAULT_EMPTY
    uline = constants.DEFAULT_EMPTY

    if tipo == constants.TYPLNE_AEREA or tipo == constants.TYPLNE_AEREA_INT:
        iNomAir = str(iNomAir_sline)
        cOHL = '1'
        new_entry = {
            'ID(a:40)': element_id,
            'loc_name(a:40)': loc_name,
            'uline(r)': uline,
            'sline(r)': sLine,
            'InomAir(r)': iNomAir,
            'cohl_(i)': cOHL,
            'rline(r)': rline,
            'xline(r)': '0',
            'rline0(r)': rline,
            'xline0(r)': '0',
            'tmax(r)': '80',
            'systp(i)': sysTp,
            'nlnph(i)': nlnph,
            'nneutral(i)': nneutral,
            'frnom(r)': '60',
            'mlei(a:2)': mlei,
            'bline(r)': '0',
            'bline0(r)': '0',
            'rnline(r)': '0',
            'rpnline(r)': '0',
            'xnline(r)': '0',
            'xpnline(r)': '0',
            'tline0(r)': '0',
            'tline(r)': '0',
            'bnline(r)': '0',
            'bpnline(r)': '0'}

    if tipo == constants.TYPLNE_SUB or tipo == constants.TYPLNE_SUB_INT:
        sLine = str(iNomAir_sline)
        cOHL = '0'
        new_entry = {
            'ID(a:40)': element_id,
            'loc_name(a:40)': loc_name,
            'uline(r)': uline,
            'sline(r)': sLine,
            'InomAir(r)': iNomAir,
            'cohl_(i)': cOHL,
            'rline(r)': rline,
            'xline(r)': '0',
            'rline0(r)': rline,
            'xline0(r)': '0',
            'tmax(r)': '80',
            'systp(i)': sysTp,
            'nlnph(i)': nlnph,
            'nneutral(i)': nneutral,
            'frnom(r)': '60',
            'mlei(a:2)': mlei,
            'bline(r)': '0',
            'bline0(r)': '0',
            'rnline(r)': '0',
            'rpnline(r)': '0',
            'xnline(r)': '0',
            'xpnline(r)': '0',
            'tline0(r)': '0',
            'tline(r)': '0',
            'bnline(r)': '0',
            'bpnline(r)': '0'}
    try:
        table.loc[index] = new_entry
    except Exception:
        print('Error: No se puede crear un tipo linea para un elemento tipo:', tipo, flush=True)
        exit(28)
    return


def create_typ_fuse(element_id, loc_name, urat, irat, index, table):
    """
    Crea el registro para los tipos fusibles dentro de la tabla TypFuse
    :param element_id: ID del registro.
    :param loc_name: Nombre del elemento.
    :param urat: Valor proveniente de Windmil.
    :param irat: Valor proveniente de Windmil.
    :param index: Posicion del registro en su respectiva tabla.
    :param table: Tabla en la que se creara el registro.
    :return: N/A.
    """
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'itype(i)': '1',
        'urat(r)': urat,
        'irat(r)': irat,
        'frq(r)': '60',
        'chartype(i)': '0'}
    table.loc[index] = new_entry
    return


def create_typ_tr2(element_id, loc_name, strn, index, table):
    """
    Crea el registro para los tipos transformadores dentro de la tabla TypTr2
    :param element_id: ID del elemento.
    :param loc_name: Nombre del elemento.
    :param strn: Valor proveniente de Windmil.
    :param index: Posicion del registro en su respectiva tabla.
    :param table: Tabla en la que se creara el registro.
    :return: N/A.
    """
    # Proviene de la columna C del archivo STD o hacer permutaciones por las 3
    # fases.
    nt2ph = constants.DEFAULT_EMPTY
    utrn_h = constants.DEFAULT_EMPTY  # Proviene de la lectura del elemento
    utrn_l = constants.DEFAULT_EMPTY  # Proviene de la lectura del elemento
    tr2cn_h = constants.DEFAULT_EMPTY
    tr2cn_l = constants.DEFAULT_EMPTY

    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'nt2ph(i)': nt2ph,
        'strn(r)': strn,
        'frnom(r)': '60',
        'utrn_h(r)': utrn_h,
        'utrn_l(r)': utrn_l,
        'uktr(r)': '0',
        'pcutr(r)': '0',
        'uk0tr(r)': '0',
        'ur0tr(r)': '0',
        'tr2cn_h(a:2)': tr2cn_h,
        'tr2cn_l(a:2)': tr2cn_l,
        'nt2ag(r)': '0',
        'curmg(r)': '0',
        'pfe(r)': '0',
        'zx0hl_n(r)': '100',
        'itapch(i)': '1',
        'rtox0_n(r)': '0',
        'itrdl(r)': '0.5',
        'itrdl_lv(r)': '0.5',
        'itrdr(r)': '0.5',
        'itrdr_lv(r)': '0.5',
        'tapchtype(i)': '0',
        'tap_side(i)': '0',
        'dutap(r)': '0',
        'phitr(r)': '0',
        'nntap0(i)': '0',
        'ntpmn(i)': '0',
        'ntpmx(i)': '0',
        'itapzdep(i)': '0',
        'itapch2(i)': '0',
        'itrldf(i)': '0',
        'itratioadpt(i)': '0'}
    table.loc[index] = new_entry
    return


def create_typ_voltreg(element_id, loc_name, In, pcReg, index, table):
    """
    Crea un registro para todos los tipos de reguladores en la tabla TypVoltreg.
    :param element_id: ID del elemento.
    :param loc_name: Nombre del elemento.
    :param In: Valor proveniente de Windmil.
    :param pcReg: Valor proveniente de Windmil.
    :param index: Posicion del registro en su respectiva tabla.
    :param table: Tabla en la que se creara el registro.
    :return: N/A
    """
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'type(i)': '0',
        'config(i)': '1',
        'frnom(r)': '60',
        'Un(r)': constants.DEFAULT_EMPTY,
        'In(r)': In,
        'pcReg(r)': pcReg,
        'nntap0(i)': '0',
        'ntapmin(i)': '0',
        'ntapmax(i)': '0',
        'uk(r)': '0',
        'pcu(r)': '0',
        'itapzdep(i)': '0'}
    table.loc[index] = new_entry
    return


def create_typ_lod(element_id, table):
    """
    Crea un registro para lso diferentes tipos de cargas en la tabla de TypLod
    Los tipos de cargas son: Monofasica, Bifasica, Trifasica.
    :param element_id: Posicion del registro en su respectiva tabla.
    :param table: Tabla en la que se creara el registro.
    :return: Ultimo ID utilizado.
    """
    loc_name = ['Carga trifásica', 'Carga bifásica', 'Carga monofásica']
    phtech = ['3', '5', '8']

    for index, name in enumerate(loc_name):
        new_entry = {
            'ID(a:40)': element_id,
            'loc_name(a:40)': name,
            "systp(i)": '0',
            'phtech(i)': phtech[index],
            'aP(r)': '0',
            'bP(r)': '0',
            'kpu0(r)': '0',
            'kpu1(r)': '1',
            'kpu(r)': '2',
            'aQ(r)': '0',
            'bQ(r)': '0',
            'kqu0(r)': '0',
            'kqu1(r)': '1',
            'kqu(r)': '2'}
        table.loc[index] = new_entry
        element_id += 1
    return element_id


def create_typ_switch(element_id, loc_name, inom, index, table):
    """
    Crea un registro para cada tipo switch dentro de la tabla TypSwitch.
    :param element_id: ID del elemento.
    :param loc_name: Nombre del elemento.
    :param inom: Valor proveniente de Windmil.
    :param index: Posicion del registro en su respectiva tabla.
    :param table: Tabla en la que se creara el registro.
    :return: N/A.
    """
    new_entry = {
        'ID(a:40)': element_id,
        'loc_name(a:40)': loc_name,
        'Inom(r)': inom,
        't_open(r)': '0',
        'R_on(r)': '0',
        'X_on(r)': '0'}
    table.loc[index] = new_entry
    return
