import app_constants as constants


def get_root_voltage(root_id, root_table, root_type, types_table):
    if root_type == constants.TYPLNE_AEREA or root_type == constants.TYPLNE_SUB_STD:
        root_type_id = root_table.loc[root_table['ID(a:40)']
                                      == root_id, 'typ_id(p)'].values[0]
        return types_table.loc[types_table['ID(a:40)'] == root_type_id, 'uline(r)'].values[0]
    elif root_type == constants.ELM_SHNT:
        return root_table.loc[root_table['ID(a:40)'] == root_id, 'ushnm(r)'].values[0]
    elif root_type == constants.ELM_VOLTREG:
        root_type_id = root_table.loc[root_table['ID(a:40)']
                                      == root_id, 'typ_id(p)'].values[0]
        return types_table.loc[types_table['ID(a:40)'] == root_type_id, 'Un(r)'].values[0]
    elif root_type == constants.TYPTR2:
        root_type_id = root_table.loc[root_table['ID(a:40)']
                                      == root_id, 'typ_id(p)'].values[0]
        return types_table.loc[types_table['ID(a:40)'] == root_type_id, 'utrn_l(r)'].values[0]
