FROM nickgryg/alpine-pandas:3.8.10

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN \
  python3 -m pip install --upgrade pip && \
  apk --no-cache --update-cache add gcc gfortran python3-dev py3-pip build-base wget freetype-dev libpng-dev openblas-dev \
  libressl-dev musl-dev libffi-dev openssl rust cargo && \
  ln -s /usr/include/locale.h /usr/include/xlocale.h && \
  apk add --no-cache postgresql-libs && \
  apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
  python3 -m pip install -r requirements.txt --no-cache-dir && \
  apk --purge del .build-deps

COPY . .

EXPOSE 8080

CMD [ "python3", "app.py" ]
