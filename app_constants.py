OHM_MILE_2_OHM_KM = 0.621371192  # Valor de conversion de ohm/milla a ohm/km
UM_PF = 4.375  # Unidad de medida en red de Powerfactory

# Nombre del archivo resultante para el modelo de PowerFactory.
FINAL_EXCEL_FILENAME = 'WIND_FACTORY.xlsx'
POWER_FACTORY_VERSION = '5.0'  # Numero de version de PowerFactory.

# Representacion del tipo linea aerea en Windmil en archivo STD.
TYPLNE_AEREA = '1'
# Representacion del tipo linea aerea en Windmil en archivo STD.
TYPLNE_AEREA_INT = 1
# Representacion del tipo linea subterranea en Windmilen archivo SEQ.
TYPLNE_SUB = '2'
# Representacion del tipo linea subterranea en Windmil en archivo STD.
TYPLNE_SUB_STD = '3'
# Representacion del tipo linea subterranea en Windmil en archivo SEQ.
TYPLNE_SUB_INT = 2

# Representacion del tipo fusible en Windmil en archivo SEQ y STD.
TYPFUSE_INT = 10
# Representacion del tipo fusible en Windmil en archivo SEQ y STD.
TYPFUSE = '10'
# Representacion del grupo para los tipos fusibles en Windmil en archivo STD.
TYPFUSE_GROUP = '5'
# Representacion del grupo para los tipos fusibles en Windmil en archivo STD.
TYPFUSE_GROUP_INT = 5

TYPLOD = '13'  # Representacion de los tipos carga en el archivo SEQ.
TYPLOD_INT = 13  # Representacion de los tipos carga en el archivo SEQ.

TYPTR2 = '5'  # Representacion de los tipos transformador en el archivo SEQ.
TYPTR2_INT = 5  # Representacion de los tipos transformador en el archivo SEQ.

TYPSWITCH = '10'  # Representacion de los tipos switch en el archivo SEQ.
TYPSWITCH_INT = 10  # Representacion de los tipos switch en el archivo SEQ.
# Representacion de los grupos admitidos para los tipos switch en el archivo SEQ.
TYPSWITCH_GROUP = ['2', '3', '4', '6', '7']
# Representacion de los grupos admitidos para los tipos switch en el archivo SEQ.
TYPSWITCH_GROUP_INT = [2, 3, 4, 6, 7]

# Representacion de los grupos admitidos para los tipos coup en el archivo SEQ.
ELM_COUP_GROUP = ['6', '10']
# Representacion de los grupos admitidos para los tipos coup en el archivo SEQ.
ELM_COUP_GROUP_INT = [6, 10]

TYPVOLTREG = '6'  # Representacion de los tipos reguladores en el archivo SEQ.
# Representacion de los tipos reguladores en el archivo SEQ.
TYPVOLTREG_INT = 6
# Representacion de los elementos reguladores en el archivo STD.
ELM_VOLTREG = '4'
# Representacion de los elementos reguladores en el archivo STD.
ELM_VOLTREG_INT = 4

ELM_SHNT = '2'  # Representacion de los elementos shnt en el archivo STD.
ELM_SHNT_INT = 2  # Representacion de los elementos shnt en el archivo STD.

ELM_XNET = '9'  # Representacion de los elementos fuente en el archivo STD.
ELM_XNET_INT = 9  # Representacion de los elementos fuente en el archivo STD.

ELM_LOD = '13'  # Representacion de los elementos carga en el archivo STD.
ELM_LOD_INT = 13  # Representacion de los elementos carga en el archivo STD.

# Representacion de los elementos linea aerea en el archivo STD.
ELM_LNE_AIR = '1'
# Representacion de los elementos linea subterranea en el archivo STD.
ELM_LNE_SUB = '3'

DEFAULT_EMPTY = ''  # Representacion de un valor null a lo largo del proceso.
