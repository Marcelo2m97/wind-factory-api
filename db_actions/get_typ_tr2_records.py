from db_connection.db_instance import db
from db_connection.typ_tr2_model import TypTr2


def get_typ_tr2_records():
    typs = []

    db.connect()

    query = TypTr2.select()
    for typ in query:
        typs.append(typ)

    db.close()

    return typs
