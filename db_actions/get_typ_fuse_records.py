from db_connection.db_instance import db
from db_connection.typ_fuse_model import TypFuse

def get_typ_fuse_records():
  
  typs = []

  db.connect()

  query = TypFuse.select()
  for typ in query:
    typs.append(typ)

  db.close()

  return typs
