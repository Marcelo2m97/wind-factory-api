from db_connection.db_instance import db
from db_connection.seq_model import Seq

def get_seq_records():

  typs = []

  db.connect()

  query = Seq.select()
  for typ in query:
    typs.append(typ)

  db.close()

  return typs
