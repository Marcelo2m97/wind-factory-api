from db_connection.typ_fuse_model import TypFuse

def save_new_typ_fuse(request_body):
  try:
    query = TypFuse.delete()
    query.execute()
  except:
    print("No se pudo borrar datos de la tabla TypFuse", flush=True)
    return False

  if request_body["typ_fuse"]:
    for typ_fuse in request_body["typ_fuse"]:
      try:
        TypFuse.create(
          IDwf = typ_fuse["ID(a:40)"],
          loc_name = typ_fuse["loc_name(a:40)"],
          itype = typ_fuse["itype(i)"],
          urat = typ_fuse["urat(r)"],
          irat = typ_fuse["irat(r)"],
          frq = typ_fuse["frq(r)"],
          chartype = typ_fuse["chartype(i)"],
        )

        print("Registro de TypFuse almacenado en la base de datos", flush=True)
      except:
        print("Registro ", typ_fuse["loc_name(a:40)"], " no pudo ser almacenado en la base de datos", flush=True)
        return False
    
    return True

  else:
    print("No se proporcionó ningún elemento TypFuse", flush=True)
    return True