from db_connection.db_instance import db
from db_connection.typ_lne_model import TypLne

def get_typ_lne_records():

  typs = []

  db.connect()

  query = TypLne.select()
  for typ in query:
    typs.append(typ)
    
  db.close()
  
  return typs
