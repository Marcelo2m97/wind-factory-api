from db_connection.seq_model import Seq

def save_new_seq_records(seq_dataframe):
  try:
    query = Seq.delete()
    query.execute()
  except:
    print("No se pudo borrar datos de la tabla Seq", flush=True)
    return False

  for index, row in seq_dataframe.iterrows():
    try:
      Seq.create(
        A = row["A"],
        B = row["B"],
        C = row["C"],
        D = row["D"],
        E = row["E"],
        F = row["F"],
        G = row["G"],
        H = row["H"],
        I = row["I"],
        J = row["J"],
        K = row["K"],
        L = row["L"],
        M = row["M"],
        N = row["N"],
        O = row["O"],
        P = row["P"],
        Q = row["Q"],
        R = row["R"],
        S = row["S"],
        T = row["T"],
        U = row["U"],
        V = row["V"],
        W = row["W"],
        X = row["X"],
        Y = row["Y"],
        Z = row["Z"],
        AA = row["AA"],
        AB = row["AB"],
        AC = row["AC"]
      )
      print("Registro ", index," de Seq almacenado en la base de datos", flush=True)
    except:
      print("Registro ", row["A"], " no pudo ser almacenado en la base de datos", flush=True)
      return False

  return True
