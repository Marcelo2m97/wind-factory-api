from db_connection.typ_lod_model import TypLod

def save_new_typ_lod(request_body):
  try:
    query = TypLod.delete()
    query.execute()
  except:
    print("No se pudo borrar datos de la tabla TypLod", flush=True)
    return False

  if request_body["typ_lod"]:
    for typ_lod in request_body["typ_lod"]:
      try:
        TypLod.create(
          IDwf = typ_lod["ID(a:40)"],
          loc_name = typ_lod["loc_name(a:40)"],
          systp = typ_lod["systp(i)"],
          phtech = typ_lod["phtech(i)"],
          aP = typ_lod["aP(r)"],
          bP = typ_lod["bP(r)"],
          kpu0 = typ_lod["kpu0(r)"],
          kpu1 = typ_lod["kpu1(r)"],
          kpu = typ_lod["kpu(r)"],
          aQ = typ_lod["aQ(r)"],
          bQ = typ_lod["bQ(r)"],
          kqu0 = typ_lod["kqu0(r)"],
          kqu1 = typ_lod["kqu1(r)"],
          kqu = typ_lod["kqu(r)"],
        )

        print("Registro de TypLod almacenado en la base de datos", flush=True)
      
      except:
        print("Registro ", typ_lod["loc_name(a:40)"], " no pudo ser almacenado en la base de datos", flush=True)
        return False
    
    return True
  
  else:
    print("No se proporcionó ningún elemento TypLod", flush=True)
    return True