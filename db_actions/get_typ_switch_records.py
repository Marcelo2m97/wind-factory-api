from db_connection.db_instance import db
from db_connection.typ_switch_model import TypSwitch

def get_typ_switch_records():
  typs = []

  db.connect()

  query = TypSwitch.select()
  for typ in query:
    typs.append(typ)

  db.close()

  return typs
