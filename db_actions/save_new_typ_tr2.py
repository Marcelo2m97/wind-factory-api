from db_connection.typ_tr2_model import TypTr2


def save_new_typ_tr2(request_body):
    try:
        query = TypTr2.delete()
        query.execute()
    except:
        print("No se pudo borrar datos de la tabla Typtr2", flush=True)
        return False

    if request_body["typ_tr2"]:
        for typ_typr2 in request_body["typ_tr2"]:
            try:
                TypTr2.create(
                    IDwf=typ_typr2["ID(a:40)"],
                    loc_name=typ_typr2["loc_name(a:40)"],
                    nt2ph=typ_typr2["nt2ph(i)"],
                    strn=typ_typr2["strn(r)"],
                    frnom=typ_typr2["frnom(r)"],
                    utrn_h=typ_typr2["utrn_h(r)"],
                    utrn_l=typ_typr2["utrn_l(r)"],
                    uktr=typ_typr2["uktr(r)"],
                    pcutr=typ_typr2["pcutr(r)"],
                    uk0tr=typ_typr2["uk0tr(r)"],
                    ur0tr=typ_typr2["ur0tr(r)"],
                    tr2cn_h=typ_typr2["tr2cn_h(a:2)"],
                    tr2cn_l=typ_typr2["tr2cn_l(a:2)"],
                    nt2ag=typ_typr2["nt2ag(r)"],
                    curmg=typ_typr2["curmg(r)"],
                    pfe=typ_typr2["pfe(r)"],
                    zx0hl_n=typ_typr2["zx0hl_n(r)"],
                    itapch=typ_typr2["itapch(i)"],
                    rtox0_n=typ_typr2["rtox0_n(r)"],
                    itrdl=typ_typr2["itrdl(r)"],
                    itrdl_lv=typ_typr2["itrdl_lv(r)"],
                    itrdr=typ_typr2["itrdr(r)"],
                    itrdr_lv=typ_typr2["itrdr_lv(r)"],
                    tapchtype=typ_typr2["tapchtype(i)"],
                    tap_side=typ_typr2["tap_side(i)"],
                    dutap=typ_typr2["dutap(r)"],
                    phitr=typ_typr2["phitr(r)"],
                    nntap0=typ_typr2["nntap0(i)"],
                    ntpmn=typ_typr2["ntpmn(i)"],
                    ntpmx=typ_typr2["ntpmx(i)"],
                    itapzdep=typ_typr2["itapzdep(i)"],
                    itapch2=typ_typr2["itapch2(i)"],
                    itrldf=typ_typr2["itrldf(i)"],
                    itratioadpt=typ_typr2["itratioadpt(i)"],
                )

                print("Registro de Typtr2 almacenado en la base de datos", flush=True)
            except:
                print("Registro ", typ_tr2["loc_name(a:40)"],
                    " TypLod no pudo ser almacenado en la base de datos", flush=True)
                return False

        return True

    else:
        #print("No se proporcionó ningún elemento Typtr2")
        return True
