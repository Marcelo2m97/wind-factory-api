from db_connection.db_instance import db
from db_connection.typ_voltreg_model import TypVoltreg

def get_typ_voltreg_records():
  typs = []

  db.connect()

  query = TypVoltreg.select()
  for typ in query:
    typs.append(typ)

  db.close()

  return typs
