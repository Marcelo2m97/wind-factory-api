from flask import json
from db_connection.typ_lne_model import TypLne

def save_new_typ_lne(request_body):
  try:
    query = TypLne.delete()
    query.execute()
  except:
    print("No se pudo borrar datos de la tabla TypLne", flush=True)
    return False
  
  if request_body["typ_lne"]:
    for typ_lne in request_body["typ_lne"]:
      try:
        TypLne.create(
          IDwf = typ_lne["ID(a:40)"],
          loc_name = typ_lne["loc_name(a:40)"],
          uline = typ_lne["uline(r)"],
          sline = typ_lne["sline(r)"],
          InomAir = typ_lne["InomAir(r)"],
          cohl = typ_lne["cohl_(i)"],
          rline = typ_lne["rline(r)"],
          xline = typ_lne["xline(r)"],
          rline0 = typ_lne["rline0(r)"],
          xline0 = typ_lne["xline0(r)"],
          tmax = typ_lne["tmax(r)"],
          systp = typ_lne["systp(i)"],
          nlnph = typ_lne["nlnph(i)"],
          nneutral = typ_lne["nneutral(i)"],
          frnom = typ_lne["frnom(r)"],
          mlei = typ_lne["mlei(a:2)"],
          bline = typ_lne["bline(r)"],
          bline0 = typ_lne["bline0(r)"],
          rnline = typ_lne["rnline(r)"],
          rpnline = typ_lne["rpnline(r)"],
          xnline = typ_lne["xnline(r)"],
          xpnline = typ_lne["xpnline(r)"],
          tline0 = typ_lne["tline0(r)"],
          tline = typ_lne["tline(r)"],
          bnline = typ_lne["bnline(r)"],
          bpnline = typ_lne["bpnline(r)"],
        )

        print("Registro de TypLne almacenado en la base de datos", flush=True)

      except:
        print("Registro ", typ_lne["loc_name(a:40)"], " no pudo ser almacenado en la base de datos", flush=True)
        return False

    return True

  else:
    print("No se proporcionó ningún elemento TypLne", flush=True)
    return True