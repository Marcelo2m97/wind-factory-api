from db_connection.typ_voltreg_model import TypVoltreg

def save_new_typ_voltreg(request_body):
  try:
    query = TypVoltreg.delete()
    query.execute()

  except:
    print("No se pudo borrar datos de la tabla TypVoltreg", flush=True)
    return False

  if request_body["typ_voltreg"]:
    for typ_voltreg in request_body["typ_voltreg"]:
      try:
        TypVoltreg.create(
          IDwf = typ_voltreg["ID(a:40)"],
          loc_name = typ_voltreg["loc_name(a:40)"],
          type = typ_voltreg["type(i)"],
          config = typ_voltreg["config(i)"],
          frnom = typ_voltreg["frnom(r)"],
          Un = typ_voltreg["Un(r)"],
          In = typ_voltreg["In(r)"],
          pcReg = typ_voltreg["pcReg(r)"],
          nntap = typ_voltreg["nntap0(i)"],
          ntapmin = typ_voltreg["ntapmin(i)"],
          ntapmax = typ_voltreg["ntapmax(i)"],
          uk = typ_voltreg["uk(r)"],
          pcu = typ_voltreg["pcu(r)"],
          itapzdep = typ_voltreg["itapzdep(i)"],
        )

        print("Registro de TypVoltreg almacenado en la base de datos", flush=True)
      except:
        print("Registro ", typ_voltreg["loc_name(a:40)"], " TypVoltreg no pudo ser almacenado en la base de datos", flush=True)
        return False

    return True

  else:
    print("No se proporcionó ningún elemento TypVoltreg", flush=True)
    return True
