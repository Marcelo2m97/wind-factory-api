from db_connection.db_instance import db
from db_connection.typ_lod_model import TypLod

def get_typ_lod_records():
  
  typs = []

  db.connect()

  query = TypLod.select()
  for typ in query:
    typs.append(typ)

  db.close()

  return typs
