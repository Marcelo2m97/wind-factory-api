from db_connection.typ_switch_model import TypSwitch

def save_new_typ_switch(request_body):
  try:
    query = TypSwitch.delete()
    query.execute()
  except:
    print("No se pudo borrar datos de la tabla TypSwitch", flush=True)
    return False

  if request_body["typ_switch"]:
    for typ_switch in request_body["typ_switch"]:
      try:
        TypSwitch.create(
          IDwf = typ_switch["ID(a:40)"],
          loc_name = typ_switch["loc_name(a:40)"],
          Inom = typ_switch["Inom(r)"],
          t_open = typ_switch["t_open(r)"],
          R_on = typ_switch["R_on(r)"],
          X_on = typ_switch["X_on(r)"],
        )
        print("Registro de TypSwitch almacenado en la base de datos", flush=True)
        
      except:
        print("Registro ", typ_switch["loc_name(a:40)"], " TypSwitch no pudo ser almacenado en la base de datos", flush=True)
        return False
    
    return True

  else:
    print("No se proporcionó ningún elemento TypSwitch", flush=True)
    return True