import app_constants as constants


def clean_types_tables(typ_lne_data, typ_tr2_data, typ_voltreg_data):
    typ_lne_data = typ_lne_data[typ_lne_data["uline(r)"]
                                != constants.DEFAULT_EMPTY]
    typ_tr2_data = typ_tr2_data[typ_tr2_data["utrn_h(r)"]
                                != constants.DEFAULT_EMPTY]
    typ_voltreg_data = typ_voltreg_data[typ_voltreg_data["Un(r)"]
                                        != constants.DEFAULT_EMPTY]
