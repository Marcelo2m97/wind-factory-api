import re

from utilities import types_translator
from utilities import elements_translator
from utilities import calculation_helper as calc

import utilities.ids as ids

import app_constants as constants

class std_assignment:

    def assign_typ_from_std(fila, tables, seq_table, types_table, std):
        """Crea el registro de tipo.

        Crea un registro en el documento final de excel donde se
        especifican las caracteristicas especificas del elemento
        correspondiente. Se obtiene la fila del archivo STD y se transforma
        en la fila con los datos que necesita PowerFactory del tipo y este
        se guarda en la hoja correspondiente.

        :param fila: Corresponde a una determinada fila del archivo STD
        :return: N/A
        """
        if fila['B'] == constants.ELM_SHNT or fila['B'] == constants.ELM_SHNT_INT:
            elements_translator.create_elm_shnt(
                element_id=ids.global_id,
                index=ids.elm_shnt_id,
                loc_name=fila['A'],
                id_elm_net=ids.elm_net_id,
                tech='2',
                rated_voltage=str(
                    float(calc.multiply_by_root3(float(fila['N'])))/1000),
                rr_power=str(
                    calc.get_reactive_power(
                        i=fila['K'],
                        j=fila['L'],
                        k=fila['M'])),
                gps_lat=fila['H'],
                gps_lon=fila['I'],
                table=tables[0])
            ids.elm_shnt_id += 1
            ids.global_id += 1
        if fila['B'] == constants.ELM_XNET or fila['B'] == constants.ELM_XNET_INT:
            elements_translator.create_elmXnet(
                element_id=ids.global_id,
                index=ids.elm_xnet_id,
                loc_name=fila['A'],
                id_elm_net=ids.elm_net_id,
                gps_lat=fila['H'],
                gps_lon=fila['I'],
                table=tables[1],
                seq_table=seq_table,
                seq_comparator_value=fila['K'])
            ids.elm_xnet_id += 1
            ids.global_id += 1
        if fila['B'] == constants.ELM_LOD or fila['B'] == constants.ELM_LOD_INT:
            elements_translator.create_elmn_lod(
                element_id=ids.global_id,
                index=ids.elm_lod_id,
                loc_name=fila['A'],
                fases=fila['C'],
                id_elm_net=ids.elm_net_id,
                plini=calc.get_reactive_power(
                    i=fila['O'],
                    j=fila['P'],
                    k=fila['Q']),
                qlini=calc.get_reactive_power(
                    i=fila['R'],
                    j=fila['S'],
                    k=fila['T']),
                out_service=calc.get_out_serv_value(
                    fila['Y']),
                classif=calc.get_load_classif(
                    fila['Y']),
                gps_lat=fila['H'],
                gps_lon=fila['I'],
                type_table=types_table[4],
                table=tables[2])
            ids.elm_lod_id += 1
            ids.global_id += 1
        
        if fila['B'] == constants.TYPTR2 or fila['B'] == constants.TYPTR2_INT:
            elm_created = elements_translator.create_elm_tr2(
                element_id=ids.global_id,
                index=ids.elm_tr2_id,
                loc_name=fila['A'],
                typ_loc_name=fila['AA'],
                id_elm_net=ids.elm_net_id,
                gps_lat=fila['H'],
                gps_lon=fila['I'],
                type_table=types_table[2],
                table=tables[3],
                nt2ph=calc.convert_tr2_phases(
                    value=fila['C']),
                utrn_h=calc.multiply_by_root3(
                    calc.get_valid_value([fila['N'], fila['M']])),
                utrn_l=calc.multiply_by_root3(
                    fila['P']),
                tr2cn_l=calc.convert_hv_side_l(
                    fila['K']),
                tr2cn_h=calc.convert_hv_side_h(
                    fila['K']))
            ids.elm_tr2_id += elm_created
            ids.global_id += elm_created
        
        if fila['B'] == constants.TYPFUSE or fila['B'] == constants.TYPFUSE_INT:
            if elements_translator.create_rel_fuse(
                    element_id=ids.global_id,
                    index=ids.rel_fuse_id,
                    loc_name=fila['A'],
                    id_elm_net=ids.elm_net_id,
                    table=tables[4],
                    comparator_value=calc.get_comparator_value([
                        fila['K'],
                        fila['L'],
                        fila['M']]),
                    types_table=types_table[1],
                    gps_lat=fila['H'],
                    gps_lon=fila['I'],
                    phases=fila['C']):
                ids.rel_fuse_id += 1
                ids.global_id += 1

        if str(fila['B']) in constants.ELM_COUP_GROUP:
            if str(fila['B']) == '10':
                if elements_translator.create_elm_coup(
                        element_id=ids.global_id,
                        index=ids.elm_coup_id,
                        loc_name=fila['A'],
                        id_elm_net=ids.elm_net_id,
                        table=tables[5],
                        comparator_value=fila['M'],
                        types_table=types_table[5],
                        nphase=calc.convert_fases(
                            fila['C']),
                        tipo=10,
                        gps_lat=fila['H'],
                        gps_lon=fila['I']):
                    ids.elm_coup_id += 1
                    ids. global_id += 1

            else:
                if elements_translator.create_elm_coup(
                        element_id=ids.global_id,
                        index=ids.elm_coup_id,
                        loc_name=fila['A'],
                        id_elm_net=ids.elm_net_id,
                        table=tables[5],
                        comparator_value=constants.DEFAULT_EMPTY,
                        types_table=types_table[5],
                        nphase=calc.convert_fases(
                            fila['C']),
                        tipo=6,
                        gps_lat=fila['H'],
                        gps_lon=fila['I']):
                    ids.elm_coup_id += 1
                    ids.global_id += 1

        if str(fila['B']) == constants.ELM_VOLTREG:
            if elements_translator.create_elm_voltreg(
                    element_id=ids.global_id,
                    index=ids.elm_voltreg_id,
                    loc_name=fila['A'],
                    id_elm_net=ids.elm_net_id,
                    table=tables[6],
                    comparator_value=calc.get_comparator_value([
                            fila['N'],
                            fila['O'],
                            fila['P']]),
                    fila_padre=calc.get_root(
                        table=std,
                        root_name=fila['D']),
                    types_table=types_table[3],
                    rated_voltage=calc.get_comparator_value([
                            fila['Q'],
                            fila['R'],
                            fila['S']]),
                    gps_lon=fila['I'],
                    gps_lat=fila['H']):
                ids.elm_voltreg_id += 1
                ids.global_id += 1
                
        if str(
                fila['B']) == constants.ELM_LNE_AIR or str(fila['B']) == constants.ELM_LNE_SUB:
            try:
                re.search(
                    r"\d+(\.\d+)?",
                    str(fila['P'])).group(0)
            except AttributeError:
                print(
                    '================================= \nERROR! en creacion de Elemento Linea: Voltaje no valido para elemento linea:',
                    fila['A'], flush=True)
                print(
                    'Valor asignado para voltaje de linea (columna P archivo STD):', fila['P'], flush=True)
                print('\n=================================', flush=True)
                exit(31)
            add_ids = elements_translator.create_elm_lne(
                element_id=ids.global_id,
                index=ids.elm_lne_id,
                loc_name=fila['A'],
                id_elm_net=ids.elm_net_id,
                table=tables[7],
                comparator_value=calc.get_comparator_value(
                    [
                        fila['K'],
                        fila['L'],
                        fila['M']]),
                types_table=types_table[0],
                nlnph=calc.convert_fases(
                    fila['C']),
                dline=str(
                    float(
                        fila['O']) / 1000),
                gps_coords_col=fila['I'],
                gps_coords_row=fila['H'],
                in_air=calc.get_in_air_value(fila['B']),
                uline=re.search(
                    r"\d+(\.\d+)?",
                    str(fila['P'])).group(0),
                nneutral=fila['AJ'])
            ids.elm_lne_id += 1
            ids.global_id += add_ids

        return
