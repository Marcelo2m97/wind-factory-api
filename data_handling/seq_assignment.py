from utilities import calculation_helper as calc
from utilities import types_translator
import utilities.ids as ids

import app_constants as constants


class seq_assignment:

    def assign_typ_from_seq(fila, tables):
        typ_lne_data, typ_fuse_data, typ_tr2_data,\
            typ_voltreg_data, typ_lod_data, typ_switch_data = tables

        """Crea el registro de tipo.

        Crea un registro en el documento final de excel donde se
        especifican las caracteristicas especificas del tipo
        correspondiente. Se obtiene la fila del archivo SEQ y se transforma
        en la fila con los datos que necesita PowerFactory del tipo y este
        se guarda en la hoja correspondiente.

        :param fila: Corresponde a una determinada fila del archivo SEQ
        :return: N/A
        """
        if fila['P'] == 'OCR':
            print('Creando OCR, Tipo:', fila['B'], ' Grupo:', fila['C'], flush=True)

        if (fila['B'] == constants.TYPLNE_AEREA or fila['B'] == constants.TYPLNE_AEREA_INT) or (
                fila['B'] == constants.TYPLNE_SUB or fila['B'] == constants.TYPLNE_SUB_INT):
            types_translator.create_typ_lne(
                element_id=ids.global_id,
                loc_name=fila['A'],
                mlei=calc.convert_mlei_value(int(fila['C'])),
                rline=calc.ConvertOhmMileToOhmKm(float(fila['E'])),
                iNomAir_sline=float(fila['D']) / 1000,
                tipo=fila['B'],
                index=ids.typ_lne_id,
                table=typ_lne_data)
            ids.global_id += 1
            ids.typ_lne_id += 1

        if (fila['B'] == constants.TYPFUSE or fila['B'] == constants.TYPFUSE_INT) and (
                fila['C'] == constants.TYPFUSE_GROUP or fila['C'] == constants.TYPFUSE_GROUP_INT or fila['P'] == 'OCR'):
            types_translator.create_typ_fuse(
                element_id=ids.global_id,
                loc_name=fila['A'],
                urat=calc.multiply_by_root3(value=fila['H']),
                irat=fila['D'],
                index=ids.typ_fuse_id,
                table=typ_fuse_data)
            ids.global_id += 1
            ids.typ_fuse_id += 1

        if fila['B'] == constants.TYPTR2 or fila['B'] == constants.TYPTR2_INT:
            types_translator.create_typ_tr2(
                element_id=ids.global_id,
                loc_name=fila['A'],
                strn=fila['K'],
                index=ids.typ_tr2_id,
                table=typ_tr2_data)
            ids.global_id += 1
            ids.typ_tr2_id += 1

        if (fila['B'] == constants.TYPSWITCH or fila['B'] == constants.TYPSWITCH_INT) and \
                (fila['C'] in constants.TYPSWITCH_GROUP or fila['C'] in constants.TYPSWITCH_GROUP_INT):
            types_translator.create_typ_switch(
                element_id=ids.global_id,
                loc_name=fila['A'],
                inom=fila['D'],
                index=ids.typ_switch_id,
                table=typ_switch_data)
            ids.global_id += 1
            ids.typ_switch_id += 1

        if fila['B'] == constants.TYPVOLTREG or fila['B'] == constants.TYPVOLTREG_INT:
            types_translator.create_typ_voltreg(
                element_id=ids.global_id,
                loc_name=fila['A'],
                In=fila['C'],
                pcReg=fila['E'],
                index=ids.typ_voltreg_id,
                table=typ_voltreg_data)
            ids.global_id += 1
            ids.typ_voltreg_id += 1

        return
