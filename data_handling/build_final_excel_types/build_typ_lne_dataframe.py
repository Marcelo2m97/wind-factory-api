from utilities import ids

def build_typ_lne_dataframe(typ_lne_dataframe, typ_lne_elements):
  for lne in typ_lne_elements:
    index = len(typ_lne_dataframe)
    typ_lne_dataframe.loc[index, "ID(a:40)"] = lne.IDwf
    typ_lne_dataframe.loc[index, "loc_name(a:40)"] = lne.loc_name
    typ_lne_dataframe.loc[index, "uline(r)"] = lne.uline
    typ_lne_dataframe.loc[index, "sline(r)"] = lne.sline
    typ_lne_dataframe.loc[index, "InomAir(r)"] = lne.InomAir
    typ_lne_dataframe.loc[index, "cohl_(i)"] = lne.cohl
    typ_lne_dataframe.loc[index, "rline(r)"] = lne.rline
    typ_lne_dataframe.loc[index, "xline(r)"] = lne.xline
    typ_lne_dataframe.loc[index, "rline0(r)"] = lne.rline0
    typ_lne_dataframe.loc[index, "xline0(r)"] = lne.xline0
    typ_lne_dataframe.loc[index, "tmax(r)"] = lne.tmax
    typ_lne_dataframe.loc[index, "systp(i)"] = lne.systp
    typ_lne_dataframe.loc[index, "nlnph(i)"] = lne.nlnph
    typ_lne_dataframe.loc[index, "nneutral(i)"] = lne.nneutral
    typ_lne_dataframe.loc[index, "frnom(r)"] = lne.frnom
    typ_lne_dataframe.loc[index, "mlei(a:2)"] = lne.mlei
    typ_lne_dataframe.loc[index, "bline(r)"] = lne.bline
    typ_lne_dataframe.loc[index, "bline0(r)"] = lne.bline0
    typ_lne_dataframe.loc[index, "rnline(r)"] = lne.rnline
    typ_lne_dataframe.loc[index, "rpnline(r)"] = lne.rpnline
    typ_lne_dataframe.loc[index, "xnline(r)"] = lne.xnline
    typ_lne_dataframe.loc[index, "xpnline(r)"] = lne.xpnline
    typ_lne_dataframe.loc[index, "tline0(r)"] = lne.tline0
    typ_lne_dataframe.loc[index, "tline(r)"] = lne.tline
    typ_lne_dataframe.loc[index, "bnline(r)"] = lne.bnline
    typ_lne_dataframe.loc[index, "bpnline(r)"] = lne.bpnline

    ids.typ_lne_id = int(lne.IDwf)
