from utilities import ids

def build_typ_tr2_dataframe(typ_switch_dataframe, typ_switch_elements):
  for tr2 in typ_switch_elements:
    index = len(typ_switch_dataframe)
    typ_switch_dataframe.loc[index, "ID(a:40)"] = tr2.IDwf
    typ_switch_dataframe.loc[index, "loc_name(a:40)"] = tr2.loc_name
    typ_switch_dataframe.loc[index, "nt2ph(i)"] = tr2.nt2ph
    typ_switch_dataframe.loc[index, "strn(r)"] = tr2.strn
    typ_switch_dataframe.loc[index, "frnom(r)"] = tr2.frnom
    typ_switch_dataframe.loc[index, "utrn_h(r)"] = tr2.utrn_h
    typ_switch_dataframe.loc[index, "utrn_l(r)"] = tr2.utrn_l
    typ_switch_dataframe.loc[index, "uktr(r)"] = tr2.uktr
    typ_switch_dataframe.loc[index, "pcutr(r)"] = tr2.pcutr
    typ_switch_dataframe.loc[index, "uk0tr(r)"] = tr2.uk0tr
    typ_switch_dataframe.loc[index, "ur0tr(r)"] = tr2.ur0tr
    typ_switch_dataframe.loc[index, "tr2cn_h(a:2)"] = tr2.tr2cn_h
    typ_switch_dataframe.loc[index, "tr2cn_l(a:2)"] = tr2.tr2cn_l
    typ_switch_dataframe.loc[index, "nt2ag(r)"] = tr2.nt2ag
    typ_switch_dataframe.loc[index, "curmg(r)"] = tr2.curmg
    typ_switch_dataframe.loc[index, "pfe(r)"] = tr2.pfe
    typ_switch_dataframe.loc[index, "zx0hl_n(r)"] = tr2.zx0hl_n
    typ_switch_dataframe.loc[index, "itapch(i)"] = tr2.itapch
    typ_switch_dataframe.loc[index, "rtox0_n(r)"] = tr2.rtox0_n
    typ_switch_dataframe.loc[index, "itrdl(r)"] = tr2.itrdl
    typ_switch_dataframe.loc[index, "itrdl_lv(r)"] = tr2.itrdl_lv
    typ_switch_dataframe.loc[index, "itrdr(r)"] = tr2.itrdr
    typ_switch_dataframe.loc[index, "itrdr_lv(r)"] = tr2.itrdr_lv
    typ_switch_dataframe.loc[index, "tapchtype(i)"] = tr2.tapchtype
    typ_switch_dataframe.loc[index, "tap_side(i)"] = tr2.tap_side
    typ_switch_dataframe.loc[index, "dutap(r)"] = tr2.dutap
    typ_switch_dataframe.loc[index, "phitr(r)"] = tr2.phitr
    typ_switch_dataframe.loc[index, "nntap0(i)"] = tr2.nntap0
    typ_switch_dataframe.loc[index, "ntpmn(i)"] = tr2.ntpmn
    typ_switch_dataframe.loc[index, "ntpmx(i)"] = tr2.ntpmx
    typ_switch_dataframe.loc[index, "itapzdep(i)"] = tr2.itapzdep
    typ_switch_dataframe.loc[index, "itapch2(i)"] = tr2.itapch2
    typ_switch_dataframe.loc[index, "itrldf(i)"] = tr2.itrldf
    typ_switch_dataframe.loc[index, "itratioadpt(i)"] = tr2.itratioadpt

    ids.typ_tr2_id = int(tr2.IDwf)
