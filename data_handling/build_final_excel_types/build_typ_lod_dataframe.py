from utilities import ids

def build_typ_lod_dataframe(typ_lod_dataframe, typ_lod_elements):
  for lod in typ_lod_elements:
    index = len(typ_lod_dataframe)
    typ_lod_dataframe.loc[index, "ID(a:40)"] = lod.IDwf
    typ_lod_dataframe.loc[index, "loc_name(a:40)"] = lod.loc_name
    typ_lod_dataframe.loc[index, "systp(i)"] = lod.systp
    typ_lod_dataframe.loc[index, "phtech(i)"] = lod.phtech
    typ_lod_dataframe.loc[index, "aP(r)"] = lod.aP
    typ_lod_dataframe.loc[index, "bP(r)"] = lod.bP
    typ_lod_dataframe.loc[index, "kpu0(r)"] = lod.kpu0
    typ_lod_dataframe.loc[index, "kpu1(r)"] = lod.kpu1
    typ_lod_dataframe.loc[index, "kpu(r)"] = lod.kpu
    typ_lod_dataframe.loc[index, "aQ(r)"] = lod.aQ
    typ_lod_dataframe.loc[index, "bQ(r)"] = lod.bQ
    typ_lod_dataframe.loc[index, "kqu0(r)"] = lod.kqu0
    typ_lod_dataframe.loc[index, "kqu1(r)"] = lod.kqu1
    typ_lod_dataframe.loc[index, "kqu(r)"] = lod.kqu

    ids.typ_lod_id = int(lod.IDwf)
