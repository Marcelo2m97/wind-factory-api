from utilities import ids

def build_typ_fuse_dataframe(typ_fuse_dataframe, typ_fuse_elements):
  for fuse in typ_fuse_elements:
    index = len(typ_fuse_dataframe)
    typ_fuse_dataframe.loc[index, "ID(a:40)"] = fuse.IDwf
    typ_fuse_dataframe.loc[index, "loc_name(a:40)"] = fuse.loc_name
    typ_fuse_dataframe.loc[index, "itype(i)"] = fuse.itype
    typ_fuse_dataframe.loc[index, "urat(r)"] = fuse.urat
    typ_fuse_dataframe.loc[index, "irat(r)"] = fuse.irat
    typ_fuse_dataframe.loc[index, "frq(r)"] = fuse.frq
    typ_fuse_dataframe.loc[index, "chartype(i)"] = fuse.chartype

    ids.typ_fuse_id = int(fuse.IDwf)
