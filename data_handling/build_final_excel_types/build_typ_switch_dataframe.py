from utilities import ids

def build_typ_switch_dataframe(typ_switch_dataframe, typ_switch_elements):
  for switch in typ_switch_elements:
    index = len(typ_switch_dataframe)
    typ_switch_dataframe.loc[index, "ID(a:40)"] = switch.IDwf
    typ_switch_dataframe.loc[index, "loc_name(a:40)"] = switch.loc_name
    typ_switch_dataframe.loc[index, "Inom(r)"] = switch.Inom
    typ_switch_dataframe.loc[index, "t_open(r)"] = switch.t_open
    typ_switch_dataframe.loc[index, "R_on(r)"] = switch.R_on
    typ_switch_dataframe.loc[index, "X_on(r)"] = switch.X_on

    ids.typ_switch_id = int(switch.IDwf)
