from utilities import ids

def build_typ_voltreg_dataframe(typ_voltreg_dataframe, typ_voltreg_elements):
  for voltreg in typ_voltreg_elements:
    index = len(typ_voltreg_dataframe)
    typ_voltreg_dataframe.loc[index, "ID(a:40)"] = voltreg.IDwf
    typ_voltreg_dataframe.loc[index, "loc_name(a:40)"] = voltreg.loc_name
    typ_voltreg_dataframe.loc[index, "type(i)"] = voltreg.type
    typ_voltreg_dataframe.loc[index, "config(i)"] = voltreg.config
    typ_voltreg_dataframe.loc[index, "frnom(r)"] = voltreg.frnom
    typ_voltreg_dataframe.loc[index, "Un(r)"] = voltreg.Un
    typ_voltreg_dataframe.loc[index, "In(r)"] = voltreg.In
    typ_voltreg_dataframe.loc[index, "pcReg(r)"] = voltreg.pcReg
    typ_voltreg_dataframe.loc[index, "nntap0(i)"] = voltreg.nntap
    typ_voltreg_dataframe.loc[index, "ntapmin(i)"] = voltreg.ntapmin
    typ_voltreg_dataframe.loc[index, "ntapmax(i)"] = voltreg.ntapmax
    typ_voltreg_dataframe.loc[index, "uk(r)"] = voltreg.uk
    typ_voltreg_dataframe.loc[index, "pcu(r)"] = voltreg.pcu
    typ_voltreg_dataframe.loc[index, "itapzdep(i)"] = voltreg.itapzdep

    ids.typ_voltreg_id = int(voltreg.IDwf)
