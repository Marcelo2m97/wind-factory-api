from data_handling.std_assignment import std_assignment

class retrieve_typs_std:

  def retrieve_typs(tables, std, seq_table, types_table):
    print("Extrayendo tipos del archivo STD...", flush=True)

    for index, row in std.iterrows():
      std_assignment.assign_typ_from_std(row, tables, seq_table, types_table, std)

    print("STD Analizado con exito.", flush=True)
