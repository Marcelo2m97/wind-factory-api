from utilities import ids
from utilities import types_translator
from utilities import elements_translator
import app_constants as constants

def create_standard_types (tables):
  ids.global_id = types_translator.create_typ_lod(element_id=ids.global_id, table=tables[4])
  ids.typ_lod_id += 3

def create_standard_elements (tables):
  elements_translator.create_gral_info(element_id=ids.global_id, table=tables[0], version=constants.POWER_FACTORY_VERSION)
  ids.global_id = elements_translator