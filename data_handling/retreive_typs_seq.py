from data_handling.seq_assignment import seq_assignment

class retrieve_typs_seq:

  def retrieve_typs(tables, seq):
    print('Extrayendo tipos del archivo SEQ...', flush=True)

    for index, row in seq.iterrows():
      seq_assignment.assign_typ_from_seq(row, tables)

    print('SEQ Analizado con exito.', flush=True)
