class json_parser:

    def parse_to_json(typsHeaders, typsContent):

        json_response = {
            "typ_lne_headers": typsHeaders[0],
            "typ_fuse_headers": typsHeaders[1],
            "typ_lod_headers": typsHeaders[2],
            "typ_tr2_headers": typsHeaders[3],
            "typ_switch_headers": typsHeaders[4],
            "typ_voltreg_headers": typsHeaders[5],
            "typ_lne_elements": typsContent[0],
            "typ_fuse_elements": typsContent[1],
            "typ_lod_elements": typsContent[2],
            "typ_tr2_elements": typsContent[3],
            "typ_switch_elements": typsContent[4],
            "typ_voltreg_elements": typsContent[5],
        }

        return json_response

# Unneeded file
