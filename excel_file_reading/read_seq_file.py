from data_frame_handling.columns_dropper import column_dropper
from data_frame_handling.headers.seq_headers import seq_headers

import pandas as pd


class read_seq_file:

    def read_seq(seq_file):
        print('Leyendo archivo SEQ...', flush=True)

        # Separamos cada elemento por comas y eliminamos la primera fila
        seq = pd.read_csv(
            seq_file,
            sep='|',
            header=None,
            skiprows=1,
            names=seq_headers.get_headers()
        )

        # Eliminamos desde la columna 29 hasta la ultima porque estan llenos de basura o datos nulos.
        seq.drop(axis=1, columns=column_dropper.drop_columns(
            29, len(seq.columns)), inplace=True)

        print('Archivo SEQ leido con exito', flush=True)

        return seq
