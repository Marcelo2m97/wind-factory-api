from data_frame_handling.columns_dropper import column_dropper
from data_frame_handling.headers.std_headers import std_headers

import app_constants as constants

import pandas as pd


class read_std_file:

    def read_std(std_file):
        # Leemos el archivo std de donde lo hemos guardado
        print("Archivo STD leido con exito", flush=True)

        # Separamos cada elemento por comas y eliminamos la primera fila
        std = pd.read_csv(
            std_file,
            sep="|",
            header=None,
            skiprows=1,
            index_col=None,
            names=std_headers.get_headers())

        print("Archivo STD leido con exito", flush=True)

        std.columns = std_headers.get_headers()

        return std
