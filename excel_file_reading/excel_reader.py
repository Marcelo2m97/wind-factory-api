import pandas as pd
import numpy as np


class excel_reader:

    def read_from_excel_file(file):
        sheet_1 = pd.read_excel(file, 'TypLne')
        sheet_2 = pd.read_excel(file, 'TypFuse')
        sheet_3 = pd.read_excel(file, 'TypLod')
        sheet_4 = pd.read_excel(file, 'Typtr2')
        sheet_5 = pd.read_excel(file, 'TypSwitch')
        sheet_6 = pd.read_excel(file, 'TypVoltreg')
        data = [
            sheet_1.values.tolist(),
            sheet_2.values.tolist(),
            sheet_3.values.tolist(),
            sheet_4.values.tolist(),
            sheet_5.values.tolist(),
            sheet_6.values.tolist(),
        ]
        return data

# Unneeded file
