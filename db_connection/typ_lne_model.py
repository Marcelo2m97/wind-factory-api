from peewee import Model, IntegerField, CharField, FloatField

from db_connection.db_instance import db

class TypLne (Model):
  IDwf = CharField()
  loc_name = CharField()
  uline = IntegerField()
  sline = FloatField()
  InomAir = FloatField()
  cohl = IntegerField()
  rline = FloatField()
  xline = FloatField()
  rline0 = FloatField()
  xline0 = FloatField()
  tmax = FloatField()
  systp = IntegerField()
  nlnph = IntegerField()
  nneutral = IntegerField()
  frnom = FloatField()
  mlei = CharField()
  bline = FloatField()
  bline0 = FloatField()
  rnline = FloatField()
  rpnline = FloatField()
  xnline = FloatField()
  xpnline = FloatField()
  tline0 = FloatField()
  tline = FloatField()
  bnline = FloatField()
  bpnline = FloatField()

  class Meta:
    database = db
