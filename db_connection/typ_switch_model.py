from peewee import FloatField, Model, CharField

from db_connection.db_instance import db

class TypSwitch (Model):
  IDwf = CharField()
  loc_name = CharField()
  Inom = FloatField()
  t_open = FloatField()
  R_on = FloatField()
  X_on = FloatField()

  class Meta:
    database = db
