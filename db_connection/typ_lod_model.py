from peewee import FloatField, Model, IntegerField, CharField

from db_connection.db_instance import db

class TypLod (Model):
  IDwf = CharField()
  loc_name = CharField()
  systp = IntegerField()
  phtech = IntegerField()
  aP = FloatField()
  bP = FloatField()
  kpu0 = FloatField()
  kpu1 = FloatField()
  kpu = FloatField()
  aQ = FloatField()
  bQ = FloatField()
  kqu0 = FloatField()
  kqu1 = FloatField()
  kqu = FloatField()

  class Meta:
    database = db
