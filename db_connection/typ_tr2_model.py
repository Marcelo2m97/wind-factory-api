from peewee import FloatField, Model, IntegerField, CharField

from db_connection.db_instance import db


class TypTr2 (Model):
    IDwf = CharField()
    loc_name = CharField()
    nt2ph = IntegerField()
    strn = FloatField()
    frnom = FloatField()
    utrn_h = FloatField()
    utrn_l = FloatField()
    uktr = FloatField()
    pcutr = FloatField()
    uk0tr = FloatField()
    ur0tr = FloatField()
    tr2cn_h = CharField()
    tr2cn_l = CharField()
    nt2ag = FloatField()
    curmg = FloatField()
    pfe = FloatField()
    zx0hl_n = FloatField()
    itapch = IntegerField()
    rtox0_n = FloatField()
    itrdl = FloatField()
    itrdl_lv = FloatField()
    itrdr = FloatField()
    itrdr_lv = FloatField()
    tapchtype = IntegerField()
    tap_side = IntegerField()
    dutap = FloatField()
    phitr = FloatField()
    nntap0 = IntegerField()
    ntpmn = IntegerField()
    ntpmx = IntegerField()
    itapzdep = IntegerField()
    itapch2 = IntegerField()
    itrldf = IntegerField()
    itratioadpt = IntegerField()

    class Meta:
        database = db
