from peewee import FloatField, Model, IntegerField, CharField

from db_connection.db_instance import db

class TypFuse (Model):
  IDwf = CharField()
  loc_name = CharField()
  itype = IntegerField()
  urat = FloatField()
  irat = FloatField()
  frq = FloatField()
  chartype = IntegerField()

  class Meta:
    database = db
