from peewee import FloatField, Model, IntegerField, CharField

from db_connection.db_instance import db

class TypVoltreg (Model):
  IDwf = CharField()
  loc_name = CharField()
  type = IntegerField()
  config = IntegerField()
  frnom = IntegerField()
  Un = FloatField()
  In = FloatField()
  pcReg = FloatField()
  nntap = IntegerField()
  ntapmin = IntegerField()
  ntapmax = IntegerField()
  uk = FloatField()
  pcu = FloatField()
  itapzdep = IntegerField()

  class Meta:
    database = db
