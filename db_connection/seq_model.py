from peewee import CharField, Model

from db_connection.db_instance import db


class Seq (Model):
  A = CharField()
  B = CharField()
  C = CharField()
  D = CharField()
  E = CharField()
  F = CharField()
  G = CharField()
  H = CharField()
  I = CharField()
  J = CharField()
  K = CharField()
  L = CharField()
  M = CharField()
  N = CharField()
  O = CharField()
  P = CharField()
  Q = CharField()
  R = CharField()
  S = CharField()
  T = CharField()
  U = CharField()
  V = CharField()
  W = CharField()
  X = CharField()
  Y = CharField()
  Z = CharField()
  AA = CharField()
  AB = CharField()
  AC = CharField()

  class Meta:
    database = db
