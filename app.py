import datetime
import json
import jwt
import os
import pandas as pd
import uuid

import app_constants as constants

from flask import Flask, request, abort, jsonify, send_file, make_response
from flask_cors import CORS, cross_origin
from functools import wraps
from dotenv import load_dotenv
from werkzeug.exceptions import BadRequest
from werkzeug.security import generate_password_hash, check_password_hash

from data_frame_handling.headers.seq_headers import seq_headers
from data_frame_handling.types_init_data_frame import init_data_frame
from data_frame_handling.complete_init_data_frame import initialize_complete_data_frame

from excel_file_reading.read_std_file import read_std_file
from excel_file_reading.read_seq_file import read_seq_file

from data_handling.retreive_typs_seq import retrieve_typs_seq
from data_handling.retrieve_typs_std import retrieve_typs_std
from data_handling.standard_elements_creation import create_standard_types
from data_handling.clean_types_tables import clean_types_tables
from data_handling.build_final_excel_types.build_seq_dataframe import build_seq_dataframe

from data_handling.build_final_excel_types.build_typ_lne_dataframe import build_typ_lne_dataframe
from data_handling.build_final_excel_types.build_typ_fuse_dataframe import build_typ_fuse_dataframe
from data_handling.build_final_excel_types.build_typ_lod_dataframe import build_typ_lod_dataframe
from data_handling.build_final_excel_types.build_typ_switch_dataframe import build_typ_switch_dataframe
from data_handling.build_final_excel_types.build_typ_tr2_dataframe import build_typ_tr2_dataframe
from data_handling.build_final_excel_types.build_typ_voltreg_dataframe import build_typ_voltreg_dataframe

from db_connection.db_instance import db
from db_connection.typ_lne_model import TypLne
from db_connection.typ_fuse_model import TypFuse
from db_connection.typ_lod_model import TypLod
from db_connection.typ_tr2_model import TypTr2
from db_connection.typ_switch_model import TypSwitch
from db_connection.typ_voltreg_model import TypVoltreg
from db_connection.seq_model import Seq

from db_actions.save_new_typ_lne import save_new_typ_lne
from db_actions.save_new_typ_fuse import save_new_typ_fuse
from db_actions.save_new_typ_lod import save_new_typ_lod
from db_actions.save_new_typ_switch import save_new_typ_switch
from db_actions.save_new_typ_tr2 import save_new_typ_tr2
from db_actions.save_new_typ_voltreg import save_new_typ_voltreg
from db_actions.save_new_seq_records import save_new_seq_records

from db_actions.get_typ_lne_records import get_typ_lne_records
from db_actions.get_typ_fuse_records import get_typ_fuse_records
from db_actions.get_typ_lod_records import get_typ_lod_records
from db_actions.get_typ_switch_records import get_typ_switch_records
from db_actions.get_typ_tr2_records import get_typ_tr2_records
from db_actions.get_typ_voltreg_records import get_typ_voltreg_records
from db_actions.get_seq_records import get_seq_records

from utilities import ids
from utilities.reset_ids import reset_ids
from utilities.create_json_response import create_json_response
from utilities.elements_translator import create_elm_net, create_gral_info, create_sta_cubic
from utilities.create_terminals import create_terminals
from utilities.assign_lowest_coords import assign_lowest_coords
from utilities.create_graphic_tables import create_graphic_tables
from utilities.clean_tables import clean_tables

app = Flask(__name__)
CORS(app)

load_dotenv()
app.config['SECRET_KEY'] = os.getenv('JWTSECRET')


# @app.before_request
# def before_request():
#     if request.endpoint != 'login' and request.method != 'OPTIONS':
#         try:
#             token = request.headers.get('Authorization').split(' ')[1]
#             result = jwt.decode(token, app.config['SECRET_KEY'])
#         except Exception as e:
#             #print(e)
#             return json.dumps({"code": "401",
#                                "message": "Unauthorized"}), \
#                 401, {'Content-Type': 'application/json; charset=utf-8'}


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods',
                         'GET,PUT,POST,DELETE,OPTIONS')
    return response


@app.teardown_request
def close_db_connection(exc):
    if not db.is_closed():
        db.close()


@app.route("/login", methods=["POST", "GET"])
def login():
    auth = request.json
    if auth:
        if auth["username"] == os.getenv("WFUSERNAME") and auth["password"] == os.getenv("WFPASSWORD"):
            token = jwt.encode({"user": auth["username"], "exp": datetime.datetime.utcnow(
            ) + datetime.timedelta(hours=6)}, app.config['SECRET_KEY'])
            return json.dumps({"token": token}), \
                200, {"Content-Type": "application/json; charset=utf-8"}

    return json.dumps({"code": "401",
                               "message": "Unauthorized"}), \
        401, {'Content-Type': 'application/json; charset=utf-8'}


@app.route("/upload-excel", methods=["POST"])
def upload_excel():

    db.connect()
    db.create_tables([Seq])

    if request.files["seq"]:
        seq_file = request.files["seq"]
        df = init_data_frame.initialize_types_data_frame()
        seq = read_seq_file.read_seq(seq_file)
        create_standard_types(df)
        retrieve_typs_seq.retrieve_typs(df, seq)
        save_new_seq_records(seq)
        typ_lne_data, typ_fuse_data, typ_tr2_data, typ_voltreg_data, typ_lod_data, typ_switch_data = df

        json_typ_lne = typ_lne_data.to_json(orient="index")
        json_typ_fuse = typ_fuse_data.to_json(orient="index")
        json_typ_lod = typ_lod_data.to_json(orient="index")
        json_typ_tr2 = typ_tr2_data.to_json(orient="index")
        json_typ_switch = typ_switch_data.to_json(orient="index")
        json_typ_voltreg = typ_voltreg_data.to_json(orient="index")

        typ_lne_elements = json.loads(json_typ_lne)
        typ_fuse_elements = json.loads(json_typ_fuse)
        typ_lod_elements = json.loads(json_typ_lod)
        typ_tr2_elements = json.loads(json_typ_tr2)
        typ_switch_elements = json.loads(json_typ_switch)
        typ_voltreg_elements = json.loads(json_typ_voltreg)

        typ_lne_response = create_json_response(
            typ_lne_elements, len(typ_lne_elements))
        typ_fuse_response = create_json_response(
            typ_fuse_elements, len(typ_fuse_elements))
        typ_lod_response = create_json_response(
            typ_lod_elements, len(typ_lod_elements))
        typ_tr2_response = create_json_response(
            typ_tr2_elements, len(typ_tr2_elements))
        typ_switch_response = create_json_response(
            typ_switch_elements, len(typ_switch_elements))
        typ_voltreg_response = create_json_response(
            typ_voltreg_elements, len(typ_voltreg_elements))

        json_final = jsonify(typ_lne=typ_lne_response, typ_fuse=typ_fuse_response, typ_lod=typ_lod_response,
                             typ_tr2=typ_tr2_response, typ_switch=typ_switch_response, typ_voltreg=typ_voltreg_response).json

        ids.global_seq = seq
        reset_ids()
        db.close()
        return json.dumps(json_final), \
            200, {"Content-Type": "application/json; charset=utf-8"}
    else:
        reset_ids()
        return json.dumps({"message": "No file was provided!"}), \
            400, {"Content-Type": "application/json; charset=utf-8"}


@app.route("/save-types", methods=["POST"])
def save_types():
    request_body = request.json

    db.connect()
    db.create_tables([TypLne, TypFuse, TypLod, TypTr2, TypSwitch, TypVoltreg])

    typ_lne_has_errors = save_new_typ_lne(request_body=request_body)
    typ_fuse_has_errors = save_new_typ_fuse(request_body=request_body)
    typ_lod_has_errors = save_new_typ_lod(request_body=request_body)
    typ_switch_has_errors = save_new_typ_switch(request_body=request_body)
    typ_tr2_has_errors = save_new_typ_tr2(request_body=request_body)
    typ_voltreg_has_errors = save_new_typ_voltreg(request_body=request_body)

    db.close()

    if not typ_lne_has_errors or not typ_fuse_has_errors or \
            not typ_lod_has_errors or not typ_switch_has_errors or\
            not typ_tr2_has_errors or not typ_voltreg_has_errors:
        return json.dumps({"error": "One of the types had an error and could not be stored in db."}), \
            400, {"Content-Type": "application/json; charset=utf-8"}

    return json.dumps({"message": "Types saved successfully in db."}), \
        201, {"Content-Type": "application/json; charset=utf-8"}


@app.route("/create-pf-file", methods=["POST"])
def create_pw_file():
    if request.files["std"]:
        std_file = request.files["std"]
        df = initialize_complete_data_frame()
        seq_h = seq_headers.get_headers()
        seq = pd.DataFrame(columns=seq_h)
        typ_lne_elements = get_typ_lne_records()
        typ_fuse_elements = get_typ_fuse_records()
        typ_lod_elements = get_typ_lod_records()
        typ_switch_elements = get_typ_switch_records()
        typ_tr2_elements = get_typ_tr2_records()
        typ_voltreg_elements = get_typ_voltreg_records()
        seq_elements = get_seq_records()

        gralData, elmNetData, intGrfNetData, elmShntData,\
            elmXnetData, elmLodData, relFuseData, elmTr2Data,\
            typLneData, typFuseData, typTr2Data, typVoltregData,\
            elmCoupData, typLodData, typSwitchData, elmVoltregData,\
            elmLneData, elmTermData, staCubicData, intGrfData,\
            intGrfconData = df

        build_typ_lne_dataframe(typLneData, typ_lne_elements)
        build_typ_fuse_dataframe(typFuseData, typ_fuse_elements)
        build_typ_lod_dataframe(typLodData, typ_lod_elements)
        build_typ_switch_dataframe(typSwitchData, typ_switch_elements)
        build_typ_tr2_dataframe(typTr2Data, typ_tr2_elements)
        build_typ_voltreg_dataframe(typVoltregData, typ_voltreg_elements)
        build_seq_dataframe(seq, seq_elements)

        create_gral_info(gralData, constants.POWER_FACTORY_VERSION)
        ids.elm_net_id = create_elm_net(elmNetData, intGrfNetData) - 1
        ids.int_grf_net_id = ids.elm_net_id + 1
        ids.global_id += ids.typ_switch_id

        elements_tables = [
            elmShntData,
            elmXnetData,
            elmLodData,
            elmTr2Data,
            relFuseData,
            elmCoupData,
            elmVoltregData,
            elmLneData,
            elmTermData
        ]
        types_tables = [
            typLneData,
            typFuseData,
            typTr2Data,
            typVoltregData,
            typLodData,
            typSwitchData
        ]

        std = read_std_file.read_std(std_file)
        retrieve_typs_std.retrieve_typs(
            elements_tables, std, seq, types_tables)

        create_terminals(elements_tables, std, types_tables)
        ids.global_id += create_sta_cubic(ids.global_id, elmTermData,
                                          ids.sta_cubic_id, staCubicData, elmTr2Data)

        assign_lowest_coords(std)
        create_graphic_tables(std, relFuseData, intGrfData,
                              elmTermData, elmCoupData, intGrfconData, elements_tables)

        writer = pd.ExcelWriter(constants.FINAL_EXCEL_FILENAME, engine='openpyxl')

        elmTermData, typLneData, typTr2Data,\
            typVoltregData, elmTr2Data = clean_tables(
                elmTermData, typLneData, typTr2Data, typVoltregData, elmTr2Data)

        print(
            '************************************************************ ESCRIBIENDO TABLAS A ARCHIVO FINAL:', constants.FINAL_EXCEL_FILENAME, ' ************************************************************', flush=True)
        print('Escribiendo hoja de General...', flush=True)
        gralData.to_excel(writer, sheet_name="General", index=False)
        print('Escribiendo hoja de ElmNet...', flush=True)
        elmNetData.to_excel(writer, sheet_name="ElmNet", index=False)
        print('Escribiendo hoja de IntGrf...', flush=True)
        intGrfData.to_excel(writer, sheet_name="IntGrf", index=False)
        print('Escribiendo hoja de IntGrfcon...', flush=True)
        intGrfconData.to_excel(writer, sheet_name="IntGrfcon", index=False)
        print('Escribiendo hoja de IntGrfnet...', flush=True)
        intGrfNetData.to_excel(writer, sheet_name="IntGrfnet", index=False)
        print('Escribiendo hoja de ElmShnt...', flush=True)
        elmShntData.to_excel(writer, sheet_name="ElmShnt", index=False)
        print('Escribiendo hoja de ElmXnet...', flush=True)
        elmXnetData.to_excel(writer, sheet_name="ElmXnet", index=False)
        print('Escribiendo hoja de ElmLod...', flush=True)
        elmLodData.to_excel(writer, sheet_name="ElmLod", index=False)
        print('Escribiendo hoja de ElmTr2...', flush=True)
        elmTr2Data.to_excel(writer, sheet_name="ElmTr2", index=False)
        print('Escribiendo hoja de RelFuse...', flush=True)
        relFuseData.to_excel(writer, sheet_name="RelFuse", index=False)
        print('Escribiendo hoja de ElmCoup...', flush=True)
        elmCoupData.to_excel(writer, sheet_name="ElmCoup", index=False)
        print('Escribiendo hoja de ElmVoltreg...', flush=True)
        elmVoltregData.to_excel(writer, sheet_name="ElmVoltreg", index=False)
        print('Escribiendo hoja de ElmLne...', flush=True)
        elmLneData.to_excel(writer, sheet_name="ElmLne", index=False)
        print('Escribiendo hoja de ElmTerm...', flush=True)
        elmTermData.to_excel(writer, sheet_name="ElmTerm", index=False)
        print('Escribiendo hoja de StaCubic...', flush=True)
        staCubicData.to_excel(writer, sheet_name="StaCubic", index=False)
        print('Escribiendo hoja de TypLne...', flush=True)
        typLneData.to_excel(writer, sheet_name="TypLne", index=False)
        print('Escribiendo hoja de TypFuse...', flush=True)
        typFuseData.to_excel(writer, sheet_name="TypFuse", index=False)
        print('Escribiendo hoja de TypLod...', flush=True)
        typLodData.to_excel(writer, sheet_name="TypLod", index=False)
        print('Escribiendo hoja de TypTr2...', flush=True)
        typTr2Data.to_excel(writer, sheet_name="TypTr2", index=False)
        print('Escribiendo hoja de TypSwitch...', flush=True)
        typSwitchData.to_excel(writer, sheet_name="TypSwitch", index=False)
        print('Escribiendo hoja de TypVoltreg...', flush=True)
        typVoltregData.to_excel(writer, sheet_name="TypVoltreg", index=False)
        writer.save()  # Escribimos todas las hojas de excel declaradas
        print(
            'Excel creado con exito. \nNombre de archivo:',
            constants.FINAL_EXCEL_FILENAME, flush=True)
        reset_ids()

        return send_file(constants.FINAL_EXCEL_FILENAME,
                        mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    else:
        no_std_file()


@app.errorhandler(BadRequest)
def no_std_file(e):
    return "No std file was provided.", 400


if __name__ == '__main__':
    # app.run(debug=True, host='0.0.0.0', port=8080, ssl_context="adhoc")
    # app.run(debug=True, host='127.0.0.1', port=5000)
    app.run(host='0.0.0.0', port=8080)
