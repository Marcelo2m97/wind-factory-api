from data_frame_handling.headers.typ_lne_headers import typ_lne_headers
from data_frame_handling.headers.typ_fuse_headers import typ_fuse_headers
from data_frame_handling.headers.typ_lod_headers import typ_lod_headers
from data_frame_handling.headers.typ_switch_headers import typ_switch_headers
from data_frame_handling.headers.typ_tr2_headers import typ_tr2_headers
from data_frame_handling.headers.typ_voltreg_headers import typ_voltreg_headers

import pandas as pd


class init_data_frame:

    def initialize_types_data_frame():
        # TypLne
        headersTypLne = typ_lne_headers.get_headers()  # Encabezados
        # DataFrame con encabezados
        dataTypLne = pd.DataFrame(columns=headersTypLne)

        # TypFuse
        headersTypFuse = typ_fuse_headers.get_headers()  # Encabezados
        # DataFrame con encabezados
        dataTypFuse = pd.DataFrame(columns=headersTypFuse)

        # TypTr2
        headersTyptr2 = typ_tr2_headers.get_headers()  # Encabezados
        # DataFrame con encabezados
        dataTyptr2 = pd.DataFrame(columns=headersTyptr2)

        # TypVoltreg
        headersTypVoltreg = typ_voltreg_headers.get_headers()  # Encabezados
        # DataFrame con encabezados
        dataTypVoltreg = pd.DataFrame(columns=headersTypVoltreg)

        # TypLod
        headersTypLod = typ_lod_headers.get_headers()  # Encabezados
        # DataFrame con encabezados
        dataTypLod = pd.DataFrame(columns=headersTypLod)

        # TypSwitch
        headersTypSwitch = typ_switch_headers.get_typ_switch_headers()  # Encabezados
        dataTypSwitch = pd.DataFrame(columns=headersTypSwitch)

        print(
            '------------------------------------------------------------------------------- Iniciando traduccion de archivos Typ -------------------------------------------------------------------------------', flush=True)

        return [
            dataTypLne,
            dataTypFuse,
            dataTyptr2,
            dataTypVoltreg,
            dataTypLod,
            dataTypSwitch,
        ]
