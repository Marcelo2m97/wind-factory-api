from utilities import excel_formatter as formatter

import pandas as pd


def initialize_complete_data_frame():
    # Gral
    gralHeaders = formatter.create_headers_gral()  # Encabezados
    gralData = pd.DataFrame(columns=gralHeaders)  # Hoja con encabezados

    # ElmNet
    elmNetHeaders = formatter.create_headers_elm_net()
    elmNetData = pd.DataFrame(columns=elmNetHeaders)

    # IntGrfNet
    intGrfNetHeaders = formatter.create_headers_int_grf_net()
    intGrfNetData = pd.DataFrame(columns=intGrfNetHeaders)

    # ElmShnt
    elmShntHeaders = formatter.create_headers_elm_shnt()
    elmShntData = pd.DataFrame(columns=elmShntHeaders)

    # ElmXnet
    elmXnetHeaders = formatter.create_headers_elmXnet()
    elmXnetData = pd.DataFrame(columns=elmXnetHeaders)

    # ElmLod
    elmLodHeaders = formatter.create_headers_elmLod()
    elmLodData = pd.DataFrame(columns=elmLodHeaders)

    # RelFuse
    relFuseHeaders = formatter.create_headers_relFuse()
    relFuseData = pd.DataFrame(columns=relFuseHeaders)

    # ElmTr2
    elmTr2Headers = formatter.create_headers_elmTr2()
    elmTr2Data = pd.DataFrame(columns=elmTr2Headers)

    # TypLne
    headersTypLne = formatter.create_headers_typ_lne()
    typLneData = pd.DataFrame(columns=headersTypLne)

    # TypFuse
    headersTypFuse = formatter.create_headers_typ_fuse()
    typFuseData = pd.DataFrame(columns=headersTypFuse)

    # TypTr2
    headersTypTr2 = formatter.create_headers_typ_tr2()
    typtr2Data = pd.DataFrame(columns=headersTypTr2)

    # TypVoltreg
    headersTypVoltreg = formatter.create_headers_typ_voltreg()
    typVoltregData = pd.DataFrame(columns=headersTypVoltreg)

    # ElmCoup
    headersElmCoup = formatter.create_headers_elmCoup()
    elmCoupData = pd.DataFrame(columns=headersElmCoup)

    # TypLod
    headersTypLod = formatter.create_headers_typ_lod()
    typLodData = pd.DataFrame(columns=headersTypLod)

    # TypSwitch
    headersTypSwitch = formatter.create_headers_typ_switch()
    typSwitchData = pd.DataFrame(columns=headersTypSwitch)

    # ElmVoltreg
    headersElmVoltreg = formatter.create_headers_elmVoltreg()
    elmVoltregData = pd.DataFrame(columns=headersElmVoltreg)

    # ElmLne
    headersElmLne = formatter.create_headers_elmLne()
    elmLneData = pd.DataFrame(columns=headersElmLne)

    # ElmTerm
    headersElmTerm = formatter.create_headers_elmTerm()
    elmTermData = pd.DataFrame(columns=headersElmTerm)

    # StaCubic
    headersStaCubic = formatter.create_headers_staCubic()
    staCubicData = pd.DataFrame(columns=headersStaCubic)

    # IntGrf
    headersIntGrf = formatter.create_headers_intGrf()
    intGrfData = pd.DataFrame(columns=headersIntGrf)

    # IntGrfCon
    headersIntGrfcon = formatter.create_headers_intGrfcon()
    intGrfconData = pd.DataFrame(columns=headersIntGrfcon)

    return [
        gralData,
        elmNetData,
        intGrfNetData,
        elmShntData,
        elmXnetData,
        elmLodData,
        relFuseData,
        elmTr2Data,
        typLneData,
        typFuseData,
        typtr2Data,
        typVoltregData,
        elmCoupData,
        typLodData,
        typSwitchData,
        elmVoltregData,
        elmLneData,
        elmTermData,
        staCubicData,
        intGrfData,
        intGrfconData
    ]
