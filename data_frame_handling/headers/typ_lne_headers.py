class typ_lne_headers:

  def get_headers ():
    typ_lne_headers = ['ID(a:40)', 'loc_name(a:40)', 'uline(r)', 'sline(r)', 'InomAir(r)', 'cohl_(i)', 'rline(r)',
      'xline(r)', 'rline0(r)', 'xline0(r)', 'tmax(r)', 'systp(i)', 'nlnph(i)', 'nneutral(i)', 'frnom(r)',
      'mlei(a:2)', 'bline(r)', 'bline0(r)', 'rnline(r)', 'rpnline(r)', 'xnline(r)', 'xpnline(r)', 'tline0(r)',
      'tline(r)', 'bnline(r)', 'bpnline(r)']
    
    return typ_lne_headers