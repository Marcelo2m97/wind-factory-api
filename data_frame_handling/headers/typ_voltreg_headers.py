class typ_voltreg_headers:

  def get_headers():
    typ_voltreg_headers = ['ID(a:40)', 'loc_name(a:40)', 'type(i)', 'config(i)', 'frnom(r)', 'Un(r)', 'In(r)', 'pcReg(r)',
                          'nntap0(i)', 'ntapmin(i)', 'ntapmax(i)', 'uk(r)', 'pcu(r)', 'itapzdep(i)']
    
    return typ_voltreg_headers
