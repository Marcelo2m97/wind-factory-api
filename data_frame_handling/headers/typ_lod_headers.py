class typ_lod_headers:
  def get_headers():
    typ_lod_headers = ['ID(a:40)', 'loc_name(a:40)', 'systp(i)', 'phtech(i)', 'aP(r)', 'bP(r)', 'kpu0(r)', 'kpu1(r)',
                      'kpu(r)', 'aQ(r)', 'bQ(r)', 'kqu0(r)', 'kqu1(r)', 'kqu(r)']
    
    return typ_lod_headers
