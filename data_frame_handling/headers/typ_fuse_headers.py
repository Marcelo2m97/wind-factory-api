class typ_fuse_headers:
  
  def get_headers():
    typ_fuse_headers = ['ID(a:40)', 'loc_name(a:40)', 'itype(i)',
                            'urat(r)', 'irat(r)', 'frq(r)', 'chartype(i)']
    
    return typ_fuse_headers
