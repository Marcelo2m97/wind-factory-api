class typ_tr2_headers:

    def get_headers():
        typ_tr2_headers = ['ID(a:40)', 'loc_name(a:40)', 'nt2ph(i)', 'strn(r)', 'frnom(r)', 'utrn_h(r)', 'utrn_l(r)',
                           'uktr(r)', 'pcutr(r)', 'uk0tr(r)', 'ur0tr(r)', 'tr2cn_h(a:2)', 'tr2cn_l(a:2)', 'nt2ag(r)', 'curmg(r)',
                           'pfe(r)', 'zx0hl_n(r)', 'itapch(i)', 'rtox0_n(r)', 'itrdl(r)', 'itrdl_lv(r)', 'itrdr(r)', 'itrdr_lv(r)',
                           'tapchtype(i)', 'tap_side(i)', 'dutap(r)', 'phitr(r)', 'nntap0(i)', 'ntpmn(i)', 'ntpmx(i)', 'itapzdep(i)',
                           'itapch2(i)', 'itrldf(i)', 'itratioadpt(i)']

        return typ_tr2_headers
